---
title: serverless-本地debug
toc: true
date: 2017-07-02 17:18:10
categories: serverless
tag: aws
---
之前，我写过一篇 {% post_link serverless/2017/serverless走起 %}，这篇是它的补充，本地调试serverless，我用的ide是vscode，下面，我们进入主题。
## serverless本地调试
首先，你需要server-offline插件，这个我们之前已经装过了，没看过上篇博客也没关系，本文不会依赖于它，但假设你已经有一个可以运行的server项目了。如果你使用身份验证，那么，你需要注意，serverless-offline目前仅支持custom authorizers，关于它的信息，你可以看我写的这篇博客: {% post_link serverless-custom-authorizers %}下面，我们进入主题。
### 修改launch.json
修改vs的调试配置，启动vscode，然后在调试下面找到 打开配置，你会发现它打开了一个配置文件：launch.json 其位于根目录下的.svcode下面，我们将它修改为：
```json
{
    "version": "0.2.0",
    "configurations": [{
        "name": "Debug",
        "type": "node",
        "request": "launch",
        "protocol": "legacy",
        "cwd": "${workspaceRoot}",
        "runtimeExecutable": "npm",
        "windows": {
            "runtimeExecutable": "npm.cmd"
        },
        "runtimeArgs": [
            "run-script",
            "debug"
        ],
        "port": 5858
    }]
}
```
### 修改package.json
现在我们修改package.json，修改后如下：
```json
{
  "name": "aws-rest-with-dynamodb",
  "version": "1.0.0",
  "description": "Serverless CRUD service exposing a REST HTTP interface",
  "author": "",
  "license": "MIT",
  "scripts": {
    "start": "./node_modules/.bin/serverless offline --skipCacheInvalidation -s dev",
    "debug": "export SLS_DEBUG=* && node --debug ./node_modules/.bin/serverless offline --skipCacheInvalidation --host 0.0.0.0 -s dev"
  },
  "dependencies": {
    "uuid": "^2.0.3"
  },
  "devDependencies": {
    "aws-sdk": "^2.78.0",
    "serverless": "^1.16.1",
    "serverless-offline": "^3.14.2"
  }
}
```
主要添加scripts部分，另外 devDependencies里添加了serverless、你需要确保你的文件里有这些。然后在serverless.yml里添加插件，改后如下：
```xml
plugins:
  - serverless-offline
```
如果你看过上篇，且也是用那个例子，那么这个插件你不用添加，之前添加过了。最后运行 npm install 依赖包将会自动下载。
最后，你在vscode里下个断点，然后点击debug或者按F5就可以调试了，当然，你还的用postman或者客户端触发你的断点。
