---
title: serverless走起
toc: true
date: 2017-07-02 14:19:15
categories: serverless
tag: aws
---
初结识serverless是几个月以前，当时我只是想了解aws的mobile hub业务，并想在android上使用它。后来，我觉得它挺有意思，当然，一直写android也未免太寂寞了，所以想学习点新的东西，总的来说，serverless是个不错的东西，也必然是以后的大势所趋，但是aws在这方面做的并不够完美，国内serverless技术也很落后，所以决定写这系列的博客，愿能帮到以后的你。<!--more-->

## serverless走起
目前提供serverless的服务商有：aws、azure、openwhisk、google cloud，另外这些网址对你会很有用。https://serverless.com 还有这个 https://github.com/awslabs 这个https://github.com/serverless/examples
我目前比较熟悉的是aws，它应该是当前云计算的老大了。这是它的文档：http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS.html 有了这些文档和例子，我相信你入门会很快。最好的办法是，下载serverless的例子，然后部署它到你的aws账号下面。之后你可以学习其他技能，比如数据库的操作，用户的操作。
### 部署一个例子
我假设你已经安装了nodejs、npm这些，且不是很老的版本，我建议使用当前的长期支持版本即可。本文假设你使用linux，至于mac和windows，其命令也相差无几，如果你不熟悉nodejs、npm，建议你先熟悉它！
现在，我们开始部署最简单的例子，终端命令如下：
```shell
npm install -g serverless
npm install serverless-offline --save-dev
git clone https://github.com/serverless/examples.git
cd examples
cd aws-node-rest-api-with-dynamodb
npm install
```
然后你需要有aws的accessKeyId和secret_access_key, 具体是访问 https://console.aws.amazon.com/iam 然后点击左边的Users tab，找到你或者新建一个User，然后点击这个User，在详情页点击security_credentials，你会有很多选项，其中之一就是Create Access key，这个key aws目前只展示一次，所以创建后你要复制好，否则等于白创建了。创建完毕后，我们创建一个deply的脚本，内容如下  
```shell
export AWS_ACCESS_KEY_ID=AKIAxxxxxxxxxxxxxxxx
export AWS_SECRET_ACCESS_KEY=CLhxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
serverless deploy
```
请自行修改ID和key，当然你也可以生成凭证，不过，我还是习惯这种临时环境变量的办法！然后给于这个deply0755的权限，接着：./deploy 你也可以指定部署到那个节点，默认应该是你账号所在的区域。编辑serverless.yml 然后改为：  
```xml
provider:
  name: aws
  runtime: nodejs4.3
  region: ap-northeast-2
```
默认是没有最后一行的。我是将它改为部署在韩国首尔。
### 本地运行
我们开发中，一般都不是直接在服务端开发的，尤其是aws的mobile hub还没有中国的节点，据说台湾的节点正在准备，但是国内没节点注定很慢很慢！！！所以直接在他上面部署测试，无疑是一场恶梦。幸好有serverless-offline插件。具体请访问：https://github.com/dherault/serverless-offline
其实上面我们已经装了插件，就剩添加到serverless.yml里了，编辑这个文件，添加：
```xml
plugins:
  - serverless-offline
```
然后我们再来写一个offline的脚本，内容如下：
```shell
export SLS_DEBUG=*
serverless offline start
```
给于权限并运行，你会发现，它在本地跑起来了，但是这里有一个坑，你一定要注意，当你offline测试的时候，不会遇到数据库读写问题也不会遇到其他权限问题（至少我没有），但是部署到aws上再运行，你就会遇到，解决这个问题也很简单，给你的角色给于相应的权限，其操作也在 https://console.aws.amazon.com/iam 里，作为serverless入门，我觉得这些基本够了。最后再教你一个招数：serverless logs -f hello -t 将 hello 改为任意的函数名，比如 update、create、list、get等，你会看到这个函数的运行日记。
### 本地调试
限于篇幅，本地调试请参考 {% post_link serverless/2017/serverless-本地debug %}
