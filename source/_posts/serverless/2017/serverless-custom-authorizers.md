---
title: serverless-custom authorizers
toc: true
date: 2017-07-02 18:23:16
categories: serverless
tag: aws
---
本文主要讲的是在Amazon API Gateway里使用自定义认证（custom authorizers），为什么是自定义认证呢？不是已经可以非常简单的和Cognito Identity集成了吗？如果已经使用了Cognito Identity，那么和它集成不是更好吗？而且也很简单。其实理由很简单，如果和Cognito Identity集成，那么你只能选择某些api不登陆不能访问，仅此而已。当然，如果已经登陆了，你可以拿到其用户信息。自定义认证，你可以设置一个api无论登陆否都可以访问，登陆了则拿到登陆信息，这个你和Cognito Identity集成是不可能完成的了。另外，最最关键的是serverless-offline 仅支持custom authorizers，仅支持！那么，如果需要本地调试，你只能选择使用custom authorizers。
## 关于Authorizers
为什么需要authorizers呢，其一是，有的api必须是登陆了才能调用的，不登陆没法调用，这是安全问题，其二，有一个方法验证登陆并把用户信息传递给接下来的方法岂不是省去了很多麻烦吗？我们接下来看看，cognito user pool提供的身份验证吧。如果我们做服务端，比如spring mvc，那么验证身份我们会给一个token，cognito user pool也类似，其文档在这里：[Using Tokens with User Pools](http://docs.aws.amazon.com/cognito/latest/developerguide/amazon-cognito-user-pools-using-tokens-with-identity-providers.html) 我们看到其提供了三个token，分别是：ID token、Access token、Refresh token，其中ID token包含了用户信息，而Access token不包含用户信息，你可以理解为ID token = Access token + Usernfo，而refresh token很明显仅能用于刷新前两者，为什么需要刷新？因为这个token是会过期的，它只有1小时的有效期，如果使用手机sdk，比如android或者ios，ID token和Access token默认仅有5分钟的有效期，但你可以设置其为30分钟，而refresh token则有30天的有效期。另外，手机端sdk会自动更新token，所以也不用担心。下面先看看android下这两个token的获取。
### android的token
请看这段代码：
```java
internal var authenticationHandler: AuthenticationHandler = object : AuthenticationHandler {
    override fun onSuccess(cognitoUserSession: CognitoUserSession?, device: CognitoDevice?) {

        val idToken = cognitoUserSession?.idToken?.jwtToken
        ILog.e("===token===", idToken)
        ILog.e("===accessToken===", cognitoUserSession?.accessToken?.jwtToken)
        ILog.e("===refreshToken===", cognitoUserSession?.refreshToken?.token)
    }

    override fun getAuthenticationDetails(authenticationContinuation: AuthenticationContinuation?, username: String?) {
        Locale.setDefault(Locale.US)
        getUserAuthentication(authenticationContinuation, username ?: userName)
    }

    override fun getMFACode(multiFactorAuthenticationContinuation: MultiFactorAuthenticationContinuation?) {

    }

    override fun onFailure(e: Exception) {
        e.printStackTrace()
    }

    override fun authenticationChallenge(continuation: ChallengeContinuation) {

     }
}
```
为什么是jwtToken呢，应为这三个token都是[JSON Web Key令牌（JWT）](https://tools.ietf.org/html/draft-ietf-jose-json-web-key-41#section-4.4)，
### 自定义验证
自定义验证很容易，这里给一个比较复杂的例子：[自定义验证和cognito user pool集成](https://aws.amazon.com/cn/blogs/mobile/integrating-amazon-cognito-user-pools-with-api-gateway/)  
1、下载这个例子[blueprint for custom authorizer for Amazon Cognito User Pools](https://s3.amazonaws.com/cup-resources/cup_custom_authorizer_lambda_function_blueprint.zip)  
2、替换authorizer.js里的配置信息：
```js
var userPoolId = ‘{REPLACE_WITH_YOUR_POOL_ID}’;
var region = ‘{REPLACE_WITH_YOUR_REGION}’;
```
然后就可以了，当然，你还需要配置下你的serverless.yml，声明你的验证类然后使用它，下面是一个例子：
```xml
notification:
  handler: push/notification.notification
  events:
    - http:
        path: push/notification
        method: post
        cors: true
        authorizer:
          name: authorizerFunc

authorizerFunc:
  handler: aws/authorizer.handler
```
### 传递用户信息
你要确认你使用的token是IDToken，默认是accessToken，你需要把 decodedJwt.payload.token_use != 这里改为id，然后修改代码如下：
```js
var authResponse = policy.build();
authResponse.context = {
    user: payload['cognito:username'],
    give_name: payload.given_name,
    phone_number: payload.phone_number,
    sub: payload.sub
};
//    callback(null, authResponse);
context.succeed(authResponse);
```
本来这里的代码是：
```js
context.succeed(policy.build());
```
但是记得不要在serverless.yml里添加：integration: lambda，至于它，请看 {% post_link serverless/2017/serverless踩坑记 %}
