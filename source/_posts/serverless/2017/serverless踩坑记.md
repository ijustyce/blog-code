---
title: serverless踩坑记
toc: true
date: 2017-07-03 19:39:38
categories: serverless
tag: aws
---
之前已经写了不少serverless相关的文章了，你可以在serverless分类下面查阅。今天的这篇是踩坑记，这些问题呢，我写这里，至于为什么这样，我不知道。主要是serverless-offline本地运行的结果和部署到aws之后运行的结果不一致，这个很坑，另外一个是自定义验证传递不了用户信息，还有本地调试ok，部署后服务器内部错误，也没有日志可查。如果你也使用serverless-offline插件，那么本文不该错过！
## serverless踩坑记
这里记录的大多跟serverless-offline有关，如果你也使用这个插件，那么推荐你读读，如果你已经是这方面的专家了，那么不用读了。
### header的大小写问题
请看[github上的这个issue](https://github.com/dherault/serverless-offline/issues/260)大小写问题。  
### 权限问题
使用serverless-offline没有任何问题，但是部署后就有问题了，归根结底是权限问题。所以你可以使用serverless logs -f function -t 查看  
### 返回值问题
当我们服务端要把数据返回给客户端时，我们用：callback(null, data),data的格式如下：
```js
{
  statusCode: 200,
  body: JSON.stringify(resultBody)
}
```
在offline测试的时候，如果resultBody是下面这样子：
```json
{ "result": { "code": 100 } }
```
那么你不用JSON.stringify也可以，但是如果部署后，那就不可以了！应该是服务器内部错误！所以我只能写JSON.stringify这个，然后resultBody传个对象即可！  
另外，我在offline环境下，如果方法的配置里有 integration: lambda 那么返回是不ok的！它返回了：
```JSON
{
  "statusCode": 200,
  "body":{
    "body": {
      "code": 102,
      "message": "verify code miss match"
    }
  }
}
```
实际需要的是：
```json
{
  "body": {
    "code": 102,
    "message": "verify code miss match"
  }
}
```
这个跟integration: lambda 配置有关，目前，我已经全面不使用它。另外，如果一个方法的配置里有它，那么event里的body直接是个对象，如果没它，那么是个JsonString。
### cache问题
具体可以参考[code outside the scope of the handler is called every function call](https://github.com/dherault/serverless-offline/issues/194)  
表现为：代码部署在aws没问题，不是handler里的代码仅仅会运行一次，或者说调用一次，但是用serverless-offline插件，却是每次都会执行，我的cache怎么办？
起初，我甚至觉得是serverless的问题，但是我清楚的记得部署在aws的代码，是只运行一次，一旦修改了它的值，下次它不会变为默认值。所以，这个问题肯定是插件的问题了。解决方案是使用 --skipCacheInvalidation选项。
