---
title: 回过头来看源码-stream
toc: true
date: 2017-03-21 15:18:28
categories: Java
tag: 回过头来看源码
---
之前写了一篇{% post_link java/回过头来看源码-Spliterator接口 %}，然而并不知道它怎么用，于是有了这篇stream，不过，现在，我不再列举javadoc里的每个方法，我只写核心东西。对于stream，javadoc里这么写的：A sequence of elements supporting sequential and parallel aggregate operations.一个元素的序列，支持顺序和并行的聚合操作。  
### stream的创建
Stream.of 和 Stream.generate都可以生成Stream对象，比如：
```java
Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5, 6);
Stream<String> stringStream = Stream.generate(()->"");
```
### stream的操作
1、distinct去重，比如：
```java
Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5, 6, 7, 1, 2, 3);

integerStream.distinct().forEach(System.out::println);
```
其最终的输出是1、2、3、4、5、6、7
2、filter，对元素进行过滤，比如
```java
Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5, 6, 7, 1, 2, 3);

integerStream.filter(num -> num < 5).forEach(System.out::println);
```
输出1、2、3、4、1、2、3
3、map，对流里面的元素根据给定的转换函数进行转换，新生成的流里面只包含转换后的元素，另外有mapToInt、mapToLong、mapToDouble
```java
Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5, 6, 7, 1, 2, 3);

integerStream.map(num -> num * 2).forEach(System.out::println);
```
4、flatMap，跟map类似，只是它生成的是新的流，而不是元素。即，它是根据给定的函数对流里面的元素进行一次转换，但是转换后的结果是该元素的流。比如上面的map，转换后的结果是新的元素，但是flatMap转换后的是新的流，你可以给这个新的流执行某些操作。
```java
ArrayList<ArrayList<String>> names = new ArrayList<>();

ArrayList<String> firstGroup = new ArrayList<>();
firstGroup.add("1");
firstGroup.add("2");
firstGroup.add("3");
names.add(firstGroup);

ArrayList<String> secondGroup = new ArrayList<>();
secondGroup.add("4");
secondGroup.add("5");
secondGroup.add("6");
names.add(secondGroup);

names.stream().flatMap(group -> group.stream().map(name -> "name" + name)).forEach(System.out::println);
```
如上，flatMap会生成多个中间流，然后分别执行后面的map操作，这也是flatMap区别与map的地方，它产生多个中间流.
5、peek，根据给定的接口函数，对流里面的元素进行调用。但并不会改变其值，比如把上面的最后一行代码改为：
```java
names.stream().flatMap(group -> group.stream().peek(System.out::println).map(name -> "name" + name)).forEach(System.out::println);
```
说到这里，你需要了解函数接口，具体请看：{% post_link java/回过头来看源码-Consumer接口 %}
6、limit，对元素进行截取前面N个，如果N比元素数多，则等于没有截取。
7、skip，跟limit类似，不过有点相反，limit是截取后取前面的，skip是取后面的，skip可以理解为跳过前面的N个元素。
8、count，返回流里面的元素个数。  
### 最后的例子：
```java
public class TestStream {

    public static void main(String[] args) {

        ArrayList<Person> personList = new ArrayList<>();
        addPerson(personList);

        personList.stream().filter(person -> person.isMan && person.age < 10).peek(person -> System.out.println(person.name))
                .map(person -> {person.name = "my name is " + person.name; System.out.println(person.name); return person;})
                .peek(person -> System.out.println(person.name)).skip(1).limit(3).forEach(person -> System.out.println(person.name));
    }

    private static void addPerson(ArrayList<Person> list) {

        for (int i = 0; i < 100; i++) {
            Person person = new Person();
            person.age = i;
            person.isMan = i % 2 == 0;
            person.name = "name" + i;
            list.add(person);
        }
    }

    private static class Person {
        public String name;
        public int age;
        public boolean isMan;
    }
}
```
这段代码说明流是并行处理，且比如map、flatmap、peek等针对的元素也是筛选之后的元素，如此，性能更好！实在佩服。
