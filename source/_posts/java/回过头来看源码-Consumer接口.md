---
title: 回过头来看源码-Consumer接口
toc: true
date: 2017-03-16 11:41:41
categories: Java
tag: 回过头来看源码
---
昨天，准备看下java里Arraylist的源码，结果发现：Arraylist继承AbstractList，然后AbstractList继承AbstractCollection，并实现list接口，list接口继承Collection，Collection继承Iterable，然后在Iterable里看到了这么几行代码：<!--more-->
```java
default void forEach(Consumer<? super T> action) {
    Objects.requireNonNull(action);
    for (T t : this) {
        action.accept(t);
    }
}
```
很好奇这个Consumer是什么，于是有了今天的文章。
### 函数式接口
java8之前，接口里的方法都是抽象方法，java8允许接口里有default和static方法，比如上面提到的Consumer接口：
```java
@FunctionalInterface
public interface Consumer<T> {

    void accept(T t);
    default Consumer<T> andThen(Consumer<? super T> after) {
        Objects.requireNonNull(after);
        return (T t) -> { accept(t); after.accept(t); };
    }
}
```
@FunctionalInterface建议你在函数式接口里添加，以免以后被误改！函数式接口有啥用呢，它可以被隐式转换为lambda表达式，这就意味着它可以友好的支持lambda表达式。不过，在介绍lambda表达式之前，先用下Consumer接口。
### 初试Consumer接口
上节的代码可能比较难以理解，尤其是那句return，如果写成如下的代码，可能你会更容易理解点。
```java
@FunctionalInterface
public interface Consumer<T> {

    void accept(T t);
    default Consumer<T> andThen(Consumer<? super T> after) {
        Objects.requireNonNull(after);
        Consumer<T> result =  (T t) -> { accept(t); after.accept(t); };
        return result;
    }
}
```
没错，(T t) -> { accept(t); after.accept(t); }; 这句的返回值就是 Consumer<T>，但不一定是Consumer，可能是其他任意接口，请看如下的代码：
```java
(Person p) -> p.age1();
IConsumer<Person> iConsumer = (Person p) -> p.age1();
```
其中，第一行代码不是语句，第二行代码没有问题，至于为什么不是语句，这样说吧，请看如下的代码：
```java
10;
int age = 10;
```
请问10是语句吗？NO！对比两段代码，其实道理一样，所以，到这里，你应该清楚我要表达什么意思了，java8引入了函数式接口这个概念，所谓函数式接口，它是只有一个抽象方法的接口，这个抽象方法调用后开始执行操作，至于default、static方法，它可以有多个。而() -> xxx 或者 () -> {xxx} 其返回值就是一个函数式接口。至此，再理解Consumer接口就没啥问题了。
最后解疑下addThen吧，比如有如下调用：
```java
Consumer<Person> personConsumer = ((Consumer<Person>) ((Person p) -> p.age1()))
        .andThen((Person p) -> p.age2());
personConsumer.accept(new Person());
```
其应该等价于：
```java
Consumer<Person> personConsumer = ((Consumer<Person>) ((Person p) -> p.age1()));
Consumer<Person> personConsumer2 = ((Consumer<Person>) ((Person p) -> p.age2()));
Person p = new Person();
personConsumer.accept(p);
personConsumer2.accept(p);
```
addThen相当于先调用当前函数接口的accept方法，接着调用下一个函数接口的accept方法，是个递归的调用。看如下的代码：
```java
Consumer<Person> personConsumer = ((Consumer<Person>) ((Person p) -> p.age1()))
        .andThen((Person p) -> p.age1())
        .andThen((Person p) -> p.age2())
        .andThen((Person p) -> p.age3())
        .andThen((Person p) -> p.age4())
        .andThen((Person p) -> p.age5());
personConsumer.accept(new Person());
```
假设这里共有6个函数接口，分别为A、B、C、D、E、F，当调用A的addThen的时候，其实先调用了A的accept，然后调用了B的accept，以此类推。最后再看一个例子。
### 对不同对象调用accept
```java
public static void main(String[] args) {

    Consumer<Integer> consumer = ((Consumer<Integer>) ((Integer value) -> printAge(value)));
    consumer.accept(1);
    consumer.accept(2);
}

private static void printAge(int age) {
    System.out.println("今年我" + age + "岁啦！ ");
}
```
我要表达的意思是，你创建的函数接口可以对不同的对象调用accept，这意味着：假设有100张图片，先压缩，然后重命名，然后转换为圆角，最后保存到本地，你可以先创建一个函数接口，然后通过一个迭代把各个图片对象传递进去，代码优雅多了。
