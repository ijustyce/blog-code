---
title: 回过头来看源码-Spliterator接口
toc: true
date: 2017-03-21 11:11:24
categories: Java
tag: 回过头来看源码
---
今天本来欲看Iterable接口的，但是却在其源码里发现了一个新的接口，没错，就是Spliterator，我很好奇这是什么，于是点进去看了下。javadoc是这么写的： An object for traversing and partitioning elements of a source.  The source of elements covered by a Spliterator could be, for example, an array, a {@link Collection}, an IO channel, or a generator function<!--more-->意思是，Spliterator是一个遍历和分割资源元素的对象，能被遍历和分割的资源对象可以是，比如，一个数组，一个集合，一个信道，一个普通的方法。本文里，暂时称Spliterator为分割迭代器。
### 解读javadoc
1、A Spliterator may traverse elements individually ({@link#tryAdvance tryAdvance()}) or sequentially in bulk ({@link #forEachRemaining forEachRemaining()}).一个分割迭代器可以单独的遍历一个元素，请看tryAdvance方法，或者遍历连续的容器里的元素，请看forEachRemaining方法。
```java
boolean tryAdvance(Consumer<? super T> action);
```
这就是前面提到的tryAdvance方法，至于其底层实现，包括面前提到的Comsumer接口，不在本博客谈论范围之内。这个方法的返回值是boolean类型，接受的参数是Consumer类型，如果action是null，会抛出异常，否则，它先执行这个action，如果还有剩余的元素则返回true，否则返回false，如果这个分割迭代器是排好序的，操作将按出现顺序在下一个元素上执行。  
2、Performs the given action for each remaining element, sequentially in the current thread, until all elements have been processed or the action throws an exception.  If this Spliterator is {@link #ORDERED}, actions are performed in encounter order.  Exceptions thrown by the action are relayed to the caller.
```java
default void forEachRemaining(Consumer<? super T> action) {
    do { } while (tryAdvance(action));
}
```
按照javadoc的说法，这个函数的作用是对每个剩余的元素执行给定的操作，在当前线程里顺序执行，直到所有元素都执行了或者操作抛出了异常。如果这个分割迭代器是顺序的，所有的操作将按照出现顺序执行，操作抛出的异常将转发给调用者。  
3、An ideal {@code trySplit} method efficiently (without traversal) divides its elements exactly in half, allowing balanced parallel computation.  Many departures from this ideal remain highly effective; for example，only approximately splitting an approximately balanced tree, or for a tree in which leaf nodes may contain either one or two elements, failing to further split these nodes.  However, large deviations in balance and/or overly inefficient {@code trySplit} mechanics typically result in poor parallel performance.
```java
Spliterator<T> trySplit();
```
一个完美的方法高效的将元素一分为二，（注意这里的exactly，精确的，恰好的），简单的讲，它是将一个分割迭代器里的元素高效的一分为二，然后并行执行，如果元素少到不能再分割，则返回null，我的理解是，假设一个分割迭代器里有100个元素，它便将其转变为两个每组有50个元素的迭代器，以便并行执行。  
3、Returns an estimate of the number of elements that would be encountered by a {@link #forEachRemaining} traversal, or returns {@link Long#MAX_VALUE} if infinite, unknown, or too expensive to compute.
```java
long estimateSize();
``` 
返回还有多少个元素需要遍历，只是个估算值，如果是无穷大、未知、或者计算起来太费时，则返回最大值（指Long类型的）！
4、Convenience method that returns {@link #estimateSize()} if this Spliterator is {@link #SIZED}, else {@code -1}.
```java
default long getExactSizeIfKnown() {
    return (characteristics() & SIZED) == 0 ? -1L : estimateSize();
}
```
便利方法，返回上面函数的值，如果这个分割迭代器是有大小的，否则返回-1，这里，说下Convenience method和Convenience class，这个值自己其实啥事都不做，只是个链接，用于提供访问其他方法的入口。请看这段英文：  
A "convenience method" or "convenience class" generally means a class that doesn't do anything itself, but provides simplified access to other classes or groups of classes. For example, the "random()" method in java.lang.Math is a convenience method that provides a simple way to use the more powerful java.util.Random class.
5、Returns a set of characteristics of this Spliterator and its elements.
```java
int characteristics();
```
返回这个分割迭代器以及它的元素的属性集。这些属性包括：ORDERED、DISTINCT、SORTED、SIZED、NONNULL、IMMUTABLE、CONCURRENT、SUBSIZED、至于具体信息，请看下篇吧。  
```java
default boolean hasCharacteristics(int characteristics) {
    return (characteristics() & characteristics) == characteristics;
}
```
检查是否含有某个属性。 
6、 If this Spliterator's source is {@link #SORTED} by a {@link Comparator},returns that {@code Comparator}. If the source is {@code SORTED} in {@linkplain Comparable natural order}, returns {@code null}.
```java 
default Comparator<? super T> getComparator() {
    throw new IllegalStateException();
}
```
如果这个分割迭代器的数据源被比较器分类好了，则返回这个比较器，如果数据源是被可比较的自然顺序分好了类，则返回null，否则抛出异常。其实，我也看不懂这段，又是Comparator，又是Comparable natural order（可比较的自然顺序）。

