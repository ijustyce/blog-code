---
title: java正则获取url参数
toc: true
date: 2016-11-15 13:13:35
categories: Java
tag: 正则
---
本文主要记录我曾经遇到的一个小问题，用正则获取url里所有参数，然后进行性能调优，如果你只需要答案，建议你复制第一段代码，如果你一定要用正则，那你请用第二段代码，第一段代码性能更好，易于理解，第二段代码更简洁。<!--more-->
## java正则获取url参数
### 性能更好的写法
```java 
public static HashMap<String, String> getUrlParams(String url){

    HashMap<String, String> map = new HashMap<>();
    String[] urls = url.split("\\?");
    if (urls.length < 1) return map;
    String keyAndValue[] = urls[1].split("&");
    if (keyAndValue.length < 1) return map;
    for (String keyValue : keyAndValue) {
        if (keyValue == null) continue;
        String[] tmp = keyValue.split("=");
        if (tmp.length < 2) continue;
        map.put(tmp[0], tmp[1]);
    }
    return map;
}
```
### 使用正则的写法
```java
private static HashMap<String, String> getUrlParams(String url) {

    HashMap<String, String> map = new HashMap<>();
    Pattern p = Pattern.compile("([^?&=]+)([=])([^&]+)");
    Matcher m = p.matcher(url);
    while (m.find()) {
        map.put(m.group(1), m.group(3));
    }
    return map;
}
```
不过，我今天要说的不是如何实现这个功能，因为这简单，我想说的是性能调优以及正则，假设url是：[http://blog.ijustyce.win/test?key1=value1&key2=value2&key3=value3](http://blog.ijustyce.win/test?key1=value1&key2=value2&key3=value3)，我需要提取的正则类似key1=value1，所以，key1里不能包含？这是第一个条件，不包含？提取出来的是key1=xxx，然后不能包含=，于是提取出来key1,接着正则继续拼配等于号，这时候剩余的字符串类似value1&key2=xxx，我要匹配的是value1，显然，它也不能包含&，所以整个正则就是：不能包含？=但接着是=，然后不能包含&，然后你会发现，第一个字符串是OK的，第二个的时候，剩余的字符串变为了&key2=xxx，所以，刚开始还不能包含&，然后就OK了，这就是我写的正则的意思，前半部分不包含？&以及=，但接着是=，第3部分不包含&，然后第一部分是key，第三部分是value，这个正则显然是没问题的。但是它的性能低，性能低也可以理解，毕竟是正则，不要对它期望过高，虽然，相对第一段代码，第二段代码每次正则都获取到了key和value，而第一段代码却是循环，但第一段代码要求很简单，或者说判断不复杂，第二段代码的正则表达式虽不很复杂但也不简单，首先第一部分不能包含？&=然后，第二部分是=，第三部分不包含&，这个条件如果用代码来实现难道要比第一段更简洁？
### 个人总结
我觉得，正则用于校验没问题，因为它会更简单，即使不用它，自己写代码也不会更高效，其次，即使我自己写代码，也不见得第一段代码比第二段高效多少，仅一条url，前者0-1毫秒，后者3-4毫秒，针对一条url调用100000次，前者355毫秒左右，后者1900毫秒左右。而且代码调用还是先调用第1段的，因为正则很可能会有缓存，调用顺序会影响执行时间的，如果先调用第二段然后第一段，1次的时候，第一段代码时间0-1毫秒，第二段3-4但是有很大的浮动，有时会跑到10多毫秒，而100000次的调用中，第一段代码仅用180毫秒左右！第二段代码时间还是老样子。
