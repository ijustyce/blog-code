---
title: 回过头来看源码-java集合
toc: true
date: 2017-03-15 14:00:16
categories: Java
tag: 回过头来看源码
---
IT技术可谓日新月异，自工作以来，学习了不少东西，就编程语言：PHP、Python、JS（vue、rn）、kotlin、swift，虽然谈不上熟练，但基本了解，如果需要，随时可以拿来用用，就我的本行Android，也是发展迅速，还记得当初仅仅是为了给我的功能机装一个记事本，我开始捣鼓手机应用，当Android走进我的世界的时候，我放弃了ACM，从此，一晃五年过去了，而我也工作三年了，Android开发从最初的eclipse到Android studio，从不了解mvc到使用mvvm，这些年发生了很多事。<!--more--> 正因为如此，这些年我基本忙于学习新的技术、框架，然后补充我自己的FastAndroidDev，很少有时间看源码。
一看源码，我瞬间傻眼了，Android开发谷歌官方目前并不支持jdk8，所以jdk8一点部不熟，在看arraylist的源码的时候，感觉自己没救了，具体请看：{% post_link java/回过头来看源码-Consumer接口 %}  
其实，这篇博文更新是回忆录，本来是想先写这篇的，结果却先写了很多
