---
title: 基于docker的mariadb多源复制实践
toc: true
date: 2018-04-11 18:17:48
categories: 服务端
tag: mariadb
---
去年加入 [ssyer.com](https://www.ssyer.com) 我也从Android工程师变身服务端工程师，从此再也不用考虑是否偏移了一两个像素，哈哈。不过随着业务的增大，我们需要在香港部署一套，很显然，香港这套系统需要和国内的系统同步数据，简单的说就是互相同步，至于数据库选型，似乎除了mariadb也没有更好的选择，Percona或许也不错，但我更相信原班人马的力量。  
## 基于docker的mariadb多源复制实践
下面进入主题，首先我们需要安装docker，然后下载mariadb的镜像，接着run一个实例，最后实现多源复制。  
### 安装docker
我是在ubuntu 16.04上进行的，首先，我们安装docker  
```shell
sudo apt install docker.io
```
接着，我们搜索下mariadb的镜像有那些，注意，这里需要root权限  
```shell
sudo docker search mariadb
```
在我的搜索结果里第一项是：
```xml
mariadb    MariaDB is a community-developed fork of M...   1881      [OK]
```
接着我们下载这个镜像，也可以跳过这步，run 的时候如果没有下载会自动去下载  
```shell
sudo docker pull mariadb
```
接着我们跑一个实例：
```shell
docker run -p 2000:3306 --name mariadb -e MYSQL_ROOT_PASSWORD=123456 -itd docker.io/mariadb:latest
```
这里 123456 是数据库 root 用户的密码，2000是本机端口，-p 2000:3306 指将本机的 2000 端口映射到 docker 实例的 3306 端口。
### 使用docker
现在我们看看 docker 实例是否在运行
```shell
sudo docker ps
```
如果列出了 mariadb 则已经在运行，如果没有列出那很可能是运行后立马退出了，一般情况下这种事很少发生在官方提供的镜像里，自己做镜像倒很有可能。现在我们进入这个docker
```shell
sudo docker exec -it mariadb /bin/bash
```
现在你已经进入到 docker 实例里面了，等于是另一个操作系统，可以为所欲为了。
### 配置my.conf
本文不涉及 mariadb 的性能优化，篇幅有限。确保已经安装了vim，然后
```shell
vim /etc/mysql/my.cnf
```
搜索 auto_increment_increment ，本文使用的是 10.2.13 在 my.conf 的 115 行附近找到了
```xml
# The following can be used as easy to replay backup logs or for replication.
# note: if you are setting up a replication slave, see README.Debian about
```
然后我修改为了：
```xml
server-id		= 1
report_host		= master1
auto_increment_increment = 2
auto_increment_offset	= 1
log_bin			= /var/log/mysql/mariadb-bin
log_bin_index		= /var/log/mysql/mariadb-bin.index
slave-skip-errors=all

binlog-do-db = ssyer
binlog-ignore-db = mysql

# not fab for performance, but safer
sync_binlog		= 1
expire_logs_days	= 10
max_binlog_size         = 100M
# slaves
relay_log		= /var/log/mysql/relay-bin
relay_log_index	= /var/log/mysql/relay-bin.index
relay_log_info_file	= /var/log/mysql/relay-bin.info
log_slave_updates
#read_only
```
这里，请注意 auto_increment_increment 和 auto_increment_offset 两个参数 如果设置了 ID 自增，那么当两个数据库同步的时候完全可能重复，这时候就很有用了，auto_increment_increment 是指每次增加的数字，一般你有几台服务器做同步你就设置为几，auto_increment_offset 是指id的初始值，比如 auto_increment_increment 均为3， A 服务器 auto_increment_offset 为 1， B服务器为2，C服务器为3，那么A服务器生成的id则是 1、4、7、10 B服务器是 2、5、8、11 C 服务器则是3、6、9、12 三台互相同步后则是 1-12 完美解决 id 冲突。 log_slave_updates 这项需要去掉注释，不然更新数据不同步， slave-skip-errors=all 是指如果同步过程发生了错误则忽略错误继续同步。binlog-do-db = ssyer 是指需要同步的数据库名字，可以有多个，binlog-ignore-db = mysql 是指 忽略的 数据库名字，mariadb 多源复制 要求不能有同名数据库，但已经忽略了默认的。
### 修改完配置后记得重启docker
先 exit 退出 docker 实例，然后 sudo docker restart mariadb
### 授权并启动同步
首先登入 mysql 控制台
```shell
mysql -u root -p
```
输入密码后即可登录，密码是创建docker实例是指定的。然后分别执行：
```shell
create database ssyer;
grant replication slave on *.* TO 'ssyer'@'%' identified BY 'ssyer';
```
执行完以上命令，会创建一个用户 ssyer 密码 ssyer 并允许在所有机器上连接，你也可以指定 % 是别的值。然后在创建一个 docker 实例B 映射到本机的 2001 端口并重复以上操作，但记得修改 my.conf 比如 server_id report_host 等等。假设先前创建的是A，现在进入A实例并执行：
```shell
show master status;
```
记录下 Position 、File 的值也可以再打开一个终端一起操作。然后登录 B 执行
```shell
change master 'm1' to MASTER_HOST='192.168.1.105', MASTER_PORT=2000, MASTER_USER='ssyer',MASTER_PASSWORD='ssyer', MASTER_LOG_FILE = 'mariadb-bin.000001', MASTER_LOG_POS=1511;
start all slaves;
show all slaves status\G;
```
这里的 mariadb-bin.000001 以及 1511 是 A 服务器上 show master status 返回的 File、Position 然后在A服务器上执行相同的操作即可。如果 show all slaves status\G; 返回的数据里有
```xml
Slave_IO_Running: Yes
Slave_SQL_Running: Yes
```
说明一切正常，可以同步。然后试试在ssyer数据库里新建一个tables 看看 另一个数据库里是否也会出现。
```sql
create table test (id int unsigned NOT NULL AUTO_INCREMENT, PRIMARY KEY ( id ), name varchar(32) ) CHARACTER SET utf8;
```
如果出现，再试试插入数据，更新数据，删除数据。比如：
```sql
insert into test (name) values ('haha');
update test set name = 'delete' where id = 1;
```
OK，今天就到这里了。