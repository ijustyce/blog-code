---
title: tensorflow入门
toc: true
date: 2017-03-09 17:33:20
categories: python
tag: 机器学习
---
学习python有一段时间了，所以，接下来很自然的进入到tensorflow了，本文将给你两个网址、一个示例还有pip以及easy_install的配置。  
## tensorflow入门
### 安装前的准备
我的环境是mac，linux类似，至于windows，请自行搜索吧，先配置easy_install和pip使用国内镜像源。  
```shell
vim ~/.pydistutils.cfg
```
并输入  
```shell
[easy_install]
index-url=http://pypi.douban.com/simple
```
然后输入mkdir ~/.pip/ 接着输入 vim ~/.pip/pip.conf 之后输入如下内容:
```shell
[global]
index-url = https://pypi.douban.com/simple
```
至此，梯子算是OK了。
### 安装tensorflow
先在终端输入：
```shell
export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/mac/cpu/tensorflow-0.12.0rc1-py3-none-any.whl
``` 
如果是linux，则输入：
```shell
export TF_BINARY_URL=https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-0.12.0rc1-cp35-cp35m-linux_x86_64.whl
```
接着：
```shell
sudo pip3 install --upgrade $TF_BINARY_URL
```
至此安装完毕。不过，请注意，这里我安装的是cpu only的版本，其区别我并没有深究，以后再补充吧。
### 测试安装
我默认你已经有合适的ide或者已经python入门了，没有的请看之前的博文，测试代码如下:
```python
import tensorflow as tf


hello = tf.constant('Hello, TensorFlow!')
sess = tf.Session()
print(sess.run(hello))


a = tf.constant(10)
b = tf.constant(32)
print(sess.run(a + b))
```
理论上，输出的是Hello, TensorFlow!和42，然而，我输出的是b'Hello, TensorFlow!'和42，似乎不聪明啊！还是？OK，这个问题也留在之后吧。最后，我们看另一个例子，识别手写的0-9的实验。
### 第一个例子
代码如下:
```shell
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("./MNIST_data/", one_hot=True)


x = tf.placeholder(tf.float32, [None, 784])
W = tf.Variable(tf.zeros([784,10]))
b = tf.Variable(tf.zeros([10]))


y = tf.nn.softmax(tf.matmul(x,W) + b)
y_ = tf.placeholder("float", [None,10]) 


cross_entropy = -tf.reduce_sum(y_*tf.log(y))


train_step = tf.train.GradientDescentOptimizer(0.01).minimize(cross_entropy)


init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)


for i in range(1000):
  batch_xs, batch_ys = mnist.train.next_batch(100)
  sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})


correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
print(sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
```
好了，今天就写这么多，目前遗留两个问题，即，为什么是b'Hello, TensorFlow!'以及安装的时候cpu和gpu的区别。