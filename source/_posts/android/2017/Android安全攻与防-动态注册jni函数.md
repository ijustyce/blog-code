---
title: 动态注册jni函数-Android安全攻与防
toc: true
date: 2017-04-13 14:39:20
categories: Android
tag: 逆向工程
---
开发一款App，如果你足够用心，肯定会想到用jni进行签名校验，当然，想偷懒用各种加固也没问题，但是用jni校验签名，照样可以破解，而且可能很简单，如果不是动态注册jni函数到java里，那么，你jni有那些函数是透明的，这时候，我只需要写一份cpp，并拥有和你一样的函数，然后替换掉你的so文件。<!--more--> 这样甚至可以让你的逻辑按我需要的来，何其危险？
### 使用jni校验签名
考虑以下代码：
```c
const char *RELEASE_SIGN = "308201d53082013ea00302010202045660fd4c300d06092a864886f70d01010******";

void exitApp(JNIEnv *env, jobject thiz) {

    jclass android = env->FindClass("android/os/Process");
    jmethodID killProcess = env->GetStaticMethodID(android, "killProcess", "(I)V");
    jmethodID myId = env->GetStaticMethodID(android, "myPid", "()I");
    env->CallStaticVoidMethod(android, killProcess, env->CallStaticIntMethod(android, myId));

    jclass system = env->FindClass("java/lang/System");
    jmethodID exit = env->GetStaticMethodID(system, "exit", "(I)V");
    env->CallStaticVoidMethod(system, exit, 0);
}

void Java_com_xxx_xxx_jni_JniUtils_checkSign(JNIEnv *env, jobject object, jobject contextObject) {
    jclass native_class = env->GetObjectClass(contextObject);
    jmethodID pm_id = env->GetMethodID(native_class, "getPackageManager","()Landroid/content/pm/PackageManager;");
    jobject pm_obj = env->CallObjectMethod(contextObject, pm_id);
    jclass pm_clazz = env->GetObjectClass(pm_obj);
    // 得到 getPackageInfo 方法的 ID
    jmethodID package_info_id = env->GetMethodID(pm_clazz, "getPackageInfo", "(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
    jclass native_classs = env->GetObjectClass(contextObject);
    jmethodID mId = env->GetMethodID(native_classs, "getPackageName","()Ljava/lang/String;");
    jstring pkg_str = static_cast<jstring>(env->CallObjectMethod(contextObject, mId));
    // 获得应用包的信息
    jobject pi_obj = env->CallObjectMethod(pm_obj, package_info_id, pkg_str, 64);
    // 获得 PackageInfo 类
    jclass pi_clazz = env->GetObjectClass(pi_obj);
    // 获得签名数组属性的 ID
    jfieldID signatures_fieldId = env->GetFieldID(pi_clazz, "signatures","[Landroid/content/pm/Signature;");
    jobject signatures_obj = env->GetObjectField(pi_obj, signatures_fieldId);
    jobjectArray signaturesArray = (jobjectArray) signatures_obj;
    jsize size = env->GetArrayLength(signaturesArray);
    jobject signature_obj = env->GetObjectArrayElement(signaturesArray, 0);
    jclass signature_clazz = env->GetObjectClass(signature_obj);
    jmethodID string_id = env->GetMethodID(signature_clazz, "toCharsString","()Ljava/lang/String;");
    jstring str = static_cast<jstring>(env->CallObjectMethod(signature_obj, string_id));
    char *c_msg = (char *) env->GetStringUTFChars(str, 0);
    if (strcmp(c_msg, RELEASE_SIGN) != 0) { //
        exitApp(env, contextObject);
    }
}
```
```java
public native String checkSign(Context context);
```
上面的代码自然能校验当前APP的签名，但是却相当于写死了函数名，别人可以自己实现你这个函数，然后生成so文件，并替换你当前的so文件，然后不就绕过签名了。
### jni_onload里动态注册、校验签名
解决以上问题的办法是使用 jni_onLoad 函数，在这里动态注册，不允许别人知道你的函数名，且在 jni_onload函数里校验签名。核心代码：
```c
static jobject getAppContext(JNIEnv* env) {
    jclass IApplication = env->FindClass("com/ijustyce/fastandroiddev3/IApplication");
    jmethodID getInstance = env->GetStaticMethodID(IApplication, "getInstance", "()Landroid/app/Application;");
    return env->CallStaticObjectMethod(IApplication, getInstance, "()I");
}
static void checkSign(JNIEnv *env, jobject object, jobject contextObject) {
    jclass native_class = env->GetObjectClass(contextObject);
    jmethodID pm_id = env->GetMethodID(native_class, "getPackageManager","()Landroid/content/pm/PackageManager;");
    jobject pm_obj = env->CallObjectMethod(contextObject, pm_id);
    jclass pm_clazz = env->GetObjectClass(pm_obj);
    // 得到 getPackageInfo 方法的 ID
    jmethodID package_info_id = env->GetMethodID(pm_clazz, "getPackageInfo", "(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
    jclass native_classs = env->GetObjectClass(contextObject);
    jmethodID mId = env->GetMethodID(native_classs, "getPackageName","()Ljava/lang/String;");
    jstring pkg_str = static_cast<jstring>(env->CallObjectMethod(contextObject, mId));
    // 获得应用包的信息
    jobject pi_obj = env->CallObjectMethod(pm_obj, package_info_id, pkg_str, 64);
    // 获得 PackageInfo 类
    jclass pi_clazz = env->GetObjectClass(pi_obj);
    // 获得签名数组属性的 ID
    jfieldID signatures_fieldId = env->GetFieldID(pi_clazz, "signatures","[Landroid/content/pm/Signature;");
    jobject signatures_obj = env->GetObjectField(pi_obj, signatures_fieldId);
    jobjectArray signaturesArray = (jobjectArray) signatures_obj;
    //  jsize size = env->GetArrayLength(signaturesArray);
    jobject signature_obj = env->GetObjectArrayElement(signaturesArray, 0);
    jclass signature_clazz = env->GetObjectClass(signature_obj);
    jmethodID string_id = env->GetMethodID(signature_clazz, "toCharsString","()Ljava/lang/String;");
    jstring str = static_cast<jstring>(env->CallObjectMethod(signature_obj, string_id));
    char *c_msg = (char *) env->GetStringUTFChars(str, 0);
    if (strcmp(c_msg, RELEASE_SIGN) != 0) {
        exitApp(env);
    }
}
jint JNI_OnLoad(JavaVM* vm, void* reserved)
{
    UnionJNIEnvToVoid uenv;
    uenv.venv = NULL;
    jint result = -1;
    JNIEnv* env = NULL;
    jobject context = NULL;

    if (vm->GetEnv(&uenv.venv, JNI_VERSION_1_6) != JNI_OK) {
        goto bail;
    }
    env = uenv.env;

    context = getAppContext(env);
    if (context == NULL) {
        exitApp(env);
    }else {
        checkSign(env, context, context);
    }

    result = JNI_VERSION_1_6;

    bail:
    return result;
}
```
然而，即便如此，也不代表你的APP绝对安全，因为攻击者可以选择不加载你的so文件，你能怎么样？不加载so文件，唯一的可能是服务端验签会失败，可你的服务端会每次都验签吗？不见得吧？所以，更安全的办法是，让你的APP离不开jni，再简单点，用jni完成网络请求，json与string互转！这样子，如果删除你的so文件那就意味着所有的网络请求都要瘫痪！！！下一篇，我们来实现这个。