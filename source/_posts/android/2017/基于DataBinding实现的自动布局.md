---
title: 基于Data Binding实现的自动布局
toc: true
date: 2017-03-10 15:39:42
categories: Android
tag: 研发周记
---
至于DataBinding是什么，这里我不再赘述，今天我要做的就是基于它实现自动布局，这么说吧，假设设计图是按照750x1334设计的，现在的手机分辨率是1500x2668，均是它的两倍，如此，原来是100x100的图标，我们应该将它变为200x200，而至于原来是100x200这种的，应该变为200x400。最后，假设现在的分辨率为2250x2668，即款是以前的3倍，高是以前的两倍，原来200x200的，在这个设备上，我让它变为了600x400，不过，<!--more-->这样做，似乎有问题，难道不应该是400x400或者600x600吗？至于到底应该是那个，请容以后再议，现在，先来实现600x400的功能吧。  
## 基于Data Binding实现的自动布局
### Data Binding里使用Glide加载图片
具体请看[这个问答](http://stackoverflow.com/questions/40465337/load-image-with-data-binding) 如果想要在data binding里使用Glide，你需要在你的model里添加如下代码：
```java
/**
 * 加载图片
 */
@BindingAdapter({"imageUrl"})
public static void loadImage(ImageView view, String imageUrl) {
    ImageLoader.load(view, imageUrl);
}
```
接着在xml里这么写：
```xml
<ImageView
    android:id="@+id/picture"
    app:imageUrl="@{item.url}"
    android:scaleType="fitXY"
    android:src="@mipmap/ic_launcher"
    android:layout_width="match_parent"
    android:layout_height="match_parent" />
```
如此，就可以了，至于上面java代码里的ImageLoader，只是我封装的图片加载库而已！为什么我要提这个呢，因为今天所说的都是基于这个！既然app:imageUrl可以实现图片的自动加载，那么app:layout_width以及app:layout_height能否实现自动设置宽高呢，答案是肯定的，毫无疑问，这下好了，这等于你的布局文件向你的model开放了一个接口，然后model实现它就可以完成自动布局了。
### 通过data binding设置控件宽高
java代码如下：
```java
//  设置宽高
@BindingAdapter("layout_width")
public static void setWidth(View view, int width) {
    if (view == null || width < 0 || !initView()) return;
    ViewGroup.LayoutParams params = view.getLayoutParams();
    if (params != null) {
        params.width = (int) (width * scaleWidth);
    }
}

@BindingAdapter("layout_height")
public static void setHeight(View view, int height) {
    if (view == null || height < 0 || !initView()) return;
    ViewGroup.LayoutParams params = view.getLayoutParams();
    if (params != null) {
        params.height = (int) (height * scaleHeight);
    }
}

@BindingAdapter({"layout_height", "layout_width"})
public static void setWidthAndHeight(View view, int height, int width) {
    if (view == null || height < 0 || width < 0 || !initView()) return;
    ViewGroup.LayoutParams params = view.getLayoutParams();
    if (params != null) {
        params.height = (int) (height * scaleHeight);
        params.width = (int) (width * scaleWidth);
    }
}
```
至于xml里，假设你这么定义:android:layout="100px"，那么，你接着写句app:layout_width="@{100}"，height类似。然后就可以了。下面是我的xml文件布局：
```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:app="http://schemas.android.com/apk/res-auto">
    <data class="AutoLayoutView">
        <variable
            name="bean"
            type="com.ijustyce.weekly1601.viewmodel.PersonView" />
    </data>
    <RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
        android:layout_width="716px"
        android:layout_height="716px"
        app:layout_width="@{716}"
        app:layout_height="@{716}"
        android:background="@color/black"
        android:orientation="vertical">

        <Button
            android:id="@+id/first"
            app:layout_width="@{200}"
            app:layout_height="@{200}"
            android:layout_marginTop="258px"
            app:layout_marginTop="@{258}"
            android:textSize="24px"
            android:text="哈哈哈哈"
            app:textSize="@{24}"
            android:layout_width="200px"
            android:layout_height="200px" />

        <Button
            android:layout_toRightOf="@id/first"
            app:layout_width="@{200}"
            app:layout_height="@{200}"
            app:textSize="@{24}"
            android:textSize="24px"
            android:text="哈哈哈哈"
            android:layout_centerVertical="true"
            android:layout_width="200px"
            android:layout_height="200px" />

        <RelativeLayout
            android:paddingLeft="200px"
            app:paddingLeft="@{200}"
            android:layout_width="match_parent"
            android:layout_height="wrap_content">

            <Button
                android:textSize="24px"
                app:textSize="@{24}"
                app:layout_width="@{200}"
                app:layout_height="@{200}"
                android:text="哈哈哈哈"
                android:layout_width="200px"
                android:layout_height="200px" />

        </RelativeLayout>

    </RelativeLayout>
</layout>
```
这部分源码已经上传至 [码云](https://git.oschina.net/ijustyce/fastandroiddev3)
### 待完善
目前，已经支持宽高、margin、padding、textsize等属性，只是支持的并不全面，比如marginStart并不支持，以及本文开始就提到的那个问题，宽是两倍，高是三倍这样的机型上该如何？我觉得，可以通过额外的属性去解决。默认宽按宽计算，高按高计算，只是允许你修改自定义。看了这么多，傻眼了吧，再不学习mvvm你就真的彻底OUT了，毫无疑问！