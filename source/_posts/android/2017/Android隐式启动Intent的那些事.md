---
title: Android隐式启动Intent的那些事
date: 2016-11-04 11:03:22
toc: true
categories: Android
---
Android开发中启动intent有两种方式，第一种是显式启动，它指定具体的类名，第二种是隐式启动，它一般指定具体的intent，本文讲述的故事就从这里开始。  
## 从AndroidManifest说起
### intent-filter标签
intent-filter用于指定这个Activity可处理的intent的属性，包括action、category、data等，其中action是这个intent的操作，比如view、pick、main等，category指分类，比如default、home等。
下面是一段AndroidManifest代码：
``` java
<intent-filter>
    <category android:name="android.intent.category.LAUNCHER" />
    <action android:name="android.intent.action.MAIN" />
</intent-filter>
<intent-filter>
    <action android:name="android.intent.action.VIEW" />
    <category android:name="android.intent.category.DEFAULT" />
    <category android:name="android.intent.category.BROWSABLE" />
    <data android:scheme="lzh" />
</intent-filter>
```
这段代码指明这个Activity是应用的入口，launcher，指显示在桌面应用列表，点击可直接打开应用，同时又可以当做浏览器，但是仅限以lzh开头的网址。预热结束，接下来讲下隐式启动intent，让我们进入主题。
### 从选择照片开始
初级程序员是这样写代码的：
``` java
Intent mIntent = new Intent(Intent.ACTION_PICK, null);
mIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
startActivityForResult(mIntent, IMAGE_PICK);
```
然后有天它发现自己写的代码可能崩溃，然后他就加了一个try、catch，变成这样：
``` java
try {
    Intent mIntent = new Intent(Intent.ACTION_PICK, null);
    mIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
    startActivityForResult(mIntent, IMAGE_PICK);
} catch (Exception e) {
    e.printStackTrace();
    ToastUtil.shortToast(R.string.image_pick_failed);
}
```
这样子的确万事大吉了，但是真的吗？不，有时候，你会发现明明你装了很多的软件，它们都可以选择照片，但是通过这几行命令总是打开其中的某一个，不给用户选择的权利，然后就将代码写成了如下：
 ``` java
 try {
     Intent mIntent = new Intent(Intent.ACTION_PICK, null);
     mIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
     Intent chooserIntent = Intent.createChooser(mIntent, "请选择您需要打开的软件");
     startActivityForResult(chooserIntent, IMAGE_PICK);
 } catch (Exception e) {
     e.printStackTrace();
     ToastUtil.shortToast(R.string.image_pick_failed);
 }
 ```
然后发现，这个catch几乎永远不会到达，即使你手机没有任何可以选择图片的软件，如果没有，系统依旧会展示一个dialog，但是提示你没有软件可以执行这个操作，于是，你可能会google no application can perform this action, 然后可能通过获取可以执行这个intent的app列表，接着判断app列表的size是否大于0，OK，这是没错，具体代码可以是：
``` java
Intent mIntent = new Intent(Intent.ACTION_PICK, null);
mIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
List<ResolveInfo> allMatches = mActivity.getPackageManager().queryIntentActivities(mIntent, PackageManager.MATCH_DEFAULT_ONLY);
if (allMatches != null && allMatches.size() > 0) {
  Intent chooserIntent = Intent.createChooser(mIntent, "请选择您需要打开的软件");
  startActivityForResult(chooserIntent, IMAGE_PICK);
}else{
  ToastUtil.shortToast(R.string.image_pick_failed);
}
```
但是，获取一整个列表有必要吗，有没有更好的办法呢？yes，我们可以通过intent的resolveActivity来解决，代码如下：
``` java
Intent mIntent = new Intent(Intent.ACTION_PICK, null);
mIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
if (mIntent.resolveActivity(mActivity.getPackageManager()) == null) {
    ToastUtil.shortToast(R.string.image_pick_failed);
    return;
}
Intent chooserIntent = Intent.createChooser(mIntent, "请选择您需要打开的软件");
startActivityForResult(chooserIntent, IMAGE_PICK);
```
至此，从加入try catch解决崩溃，然后到解决不能自主选择app，接着解决没有软件执行这个操作时的逻辑，这个隐式启动的问题完美解决，以后再补充点隐式启动intent可以完成的操作，比如分享、打开地图并展示某个点、发送邮件、打开浏览器、发送短信、拨打电话等。
