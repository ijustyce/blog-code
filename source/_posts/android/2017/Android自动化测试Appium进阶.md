---
title: Android自动化测试Appium进阶
date: 2016-11-04 13:30:20
categories: Android
tag: appium
---
今天一连更新多篇博文，现在，让我们再次走进Appium，曾经写过一篇Appium入门的文章，请自行搜索，今天，主要是针对一些常用操作，比如滑动、比如检测运行时授权，的封装。封装后的代码如下：<!--more-->
``` java
package com.lzhplus.app;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by yangchun on 2016/9/26.
 */
public class Tool {

    private static AppiumDriver driver;
    private static int timeOut = 2; //  默认重试两次
    public static WebElement id(String id) {
        return id(id, timeOut);
    }

    public static WebElement id(String id, int timeOut){
        WebElement webElement = findById(id);
        while (webElement == null && timeOut > 0){
            sleep(1000);
            webElement = findById(id);
            timeOut--;
        }
        return webElement;
    }

    private static WebElement findById(String id){
        try {
            return driver.findElementById(id);
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean idShow(String id) {
        WebElement webElement = id(id);
        return webElement != null && webElement.isDisplayed();
    }

    public static boolean clickId(String id) {
        WebElement webElement = id(id);
        if (webElement != null && webElement.isDisplayed()) {
            webElement.click();
            sleep(1000);
            return true;
        }
        return false;
    }

    public static void back(){
        driver.navigate().back();
        sleep(2000);
    }

    public static void sleep(int millis){
        try {
            Thread.sleep(millis);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static boolean intputId(String id, String value) {
        WebElement webElement = id(id);
        if (webElement != null) {
            webElement.sendKeys(value);
            sleep(2000);
            return true;
        }
        return false;
    }

    /**
     * 复制文件
     *
     * @param newPath 目标文件路径
     * @return 成功返回true，失败返回false
     */
    private static boolean copyFile(File oldfile, String newPath) {

        if (oldfile == null || newPath == null) return false;
        try {
            int bytesum = 0;
            int byteread;
            if (oldfile.exists()) { //文件存在时
                InputStream inStream = new FileInputStream(oldfile); //读入原文件
                FileOutputStream fs = new FileOutputStream(newPath);
                byte[] buffer = new byte[1444];
                while ((byteread = inStream.read(buffer)) != -1) {
                    bytesum += byteread; //字节数 文件大小
                    fs.write(buffer, 0, byteread);
                }
                fs.close();
                inStream.close();
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;

        }
        return true;
    }

    public static void capture() {
        String currentPath = System.getProperty("user.dir");
        File scrFile = driver.getScreenshotAs(OutputType.FILE);
        String filename = System.currentTimeMillis() + ".png";
        currentPath = currentPath + "/capture/" + getDateString("yyyy-MM-dd") + "/";
        File f = new File(currentPath);
        if (!f.exists()) f.mkdirs();
        currentPath = currentPath + filename;
        log("save snapshot path is:" + currentPath);
        copyFile(scrFile, currentPath);
    }

    public static void setDriver(AppiumDriver driver) {
        Tool.driver = driver;
    }

    public static void setTimeOut(int timeOut){
        Tool.timeOut = timeOut;
    }

    public static void log(String text) {
        System.out.println("===" + getDateString("yyyy-MM-dd HH:mm:ss") + "===,  " + text);
    }

    public static void scrollUp(int during){
        int width = driver.manage().window().getSize().width;
        int height = driver.manage().window().getSize().height;
        driver.swipe(width / 2, height * 3 / 4, width / 2, height / 4, during);
    }

    public static void scrollDown(int during){
        int width = driver.manage().window().getSize().width;
        int height = driver.manage().window().getSize().height;
        driver.swipe(width / 2, height / 4, width / 2, height * 3 / 4, during);
    }

    public static void scrollLeft(int during){
        int width = driver.manage().window().getSize().width;
        int height = driver.manage().window().getSize().height;
        driver.swipe(width * 3 / 4, height / 2, width / 4, height / 2, during);
    }

    public static void scrollRight(int during){
        int width = driver.manage().window().getSize().width;
        int height = driver.manage().window().getSize().height;
        driver.swipe(width / 4, height / 2, width * 3 / 4, height / 2, during);
    }

    public static String getDateString(String format) {

        SimpleDateFormat ft = new SimpleDateFormat(format, Locale.getDefault());
        Date dd = new Date();
        return ft.format(dd);
    }

    public static void checkPermission(){
        if (Tool.clickId("com.android.packageinstaller:id/permission_allow_button")){
            checkPermission();
        }
    }

    public static void checkAlert(){
        if (Tool.clickId("android:id/switchWidget")){
            back();
            sleep(3000);
        }
    }
}
```
其中checkPermission是检测界面是否有Android6.0以后的动态授权dialog、有则允许，checkAlert是检测是否有是否允许显示悬浮窗的提示，有则允许并返回。
