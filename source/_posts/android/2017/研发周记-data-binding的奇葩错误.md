---
title: 研发周记-data binding的奇葩错误
toc: true
date: 2017-04-20 10:23:28
categories: Android
tag: 研发周记
---
使用data binding已经有段时间了，现在准备全面使用mvvm，把以前的旧代码彻底重构掉！于是乎遇到了几个常见错误。比如：Exception while handling step android.databinding.annotationprocessor.ProcessExpressions@572da56d javax.xml.bind.UnmarshalException以及啥错误信息也没有，就是找不到包等。  
## data binding的常见错误
### 在Windows系统下水土不服
这主要指刚开始提到的第一个问题，没错，又是中文支持问题，比如：
```xml
<TextView
    android:id="@+id/name"
    app:layout_marginTop="@{18}"
    app:textSize="@{26}"
    style="@style/my_4_state_tv"
    android:text='@{"您好, " + item.name}' />
```
这样写linux、mac没问题，但是windows就报错了，很长很长的报错，主要原因是中文字符串的拼接，你改为：
```xml
<TextView
    android:id="@+id/name"
    app:layout_marginTop="@{18}"
    app:textSize="@{26}"
    style="@style/my_4_state_tv"
    android:text="@{@string/hi + item.name}" />
```
就没问题了，但是，如果你的项目是在mac或linux下写的，突然换到windows的，而且里面有大量的字符串拼接，天呐，那简直是一场灾难！
### 没有id会提示你找不到包
这个很简单如果上面的代码没有id，即删除 android:id="@+id/name" 不会提示你具体的错误，但就是找不到包等等，摸不着头脑！
其中，windows下中文拼接的问题，我是在Android Studio 2.3.1以及2.2.3都遇到，至于2.4没测试过，至于最后的问题，Android studio2.2.3以及最新的2.4.6 preview都有问题。
### 一点建议
或许这部分没有必要，但还是写写吧，至于中文拼接问题，你可以在model里新增一个方法，比如getWelcomeText();然后返回你好以及姓名，我不建议你写到string里，然后再拼接，当然，如果你的应用要考虑多语言支持，你可以在getWelcomeText里将你好换作string里的资源，尽量保持xml的整洁。另外，比如你的热修复不支持xml的修复，你也可以通过修改model完成bug的修复，假如你有这样的需求！  
最后，每个view，如果不要必须，那么别加id，但如果用到了数据绑定，你不加id，系统也找不到这个view吧？如何完成绑定呢？