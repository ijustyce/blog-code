---
title: Android上传aar到nexus服务器
toc: true
date: 2016-12-01 17:50:00
categories: Android
---
最近遇到一个这样的需求，我的项目有很多的模块，比如：通用工具、umeng分享、umeng推送、支付宝、百川电商sdk、环信sdk等等，共计8-9个模块吧，每次构建都很慢，因为每个模块都需要构建，尤其是clean后build，那更慢，于是，我想着通过aar来试试，因为很多模块不会改，基本是不改的，所以直接打包成aar就可以了，但是，遇到了不少问题，最后，我决定搭建本地私服。<!--more-->  
我遇到的问题主要是，假设A依赖B，B依赖某个aar文件，如果你用
```java
repositories {
    flatDir {
        dirs 'libs'
    }
}
```
这个代码以实现编译本地aar，你会A里无法调用aar里的api，只能B里调用，这种感觉就是依赖没有传递，这是很差的体验，所以，我不得不搭建本地私服，即nexus服务器！
## 上传aar到本地nexus服务器
### 关于nexus
[官网](https://www.sonatype.com/download-oss-sonatype)  
2.x这3.x最大的区别我觉得是3.x不能直接在浏览器里浏览，会提示：This maven2 hosted repository is not directly browseable at this URL. 我安装的是3.x，下载后解压。
### 运行nexus
解压后得到一个文件夹：nexus-xxx，它里面有两个文件夹：nexus-xxx和sonatype-work，打开其中的nexus-xxx，里面有bin、etc等目录，etc目录下的nexus-default.properties是它的配置文件，你可以修改端口，IP不建议你修改！然后转到bin目录，输入：./nexus start 然后你就等吧，虽然它立马返回启动了，实际没有，你就等几分钟吧，期间可以一直刷 ip:端口 默认是 127.0.0.1:8081 
### 新建仓库
我假设你已经启动，你需要先登录，默认帐号密码：admin、admin123，然后你会发现左边多了一个类似设置的按钮，点击它，左边出现Repository，点击它下面的repositories，然后点击Create repository，在列表里选中maven2 hosted，填入信息即可！
### Android studio上传aar到本地仓库
先使用插件
```xml
apply plugin: 'maven'
```
然后添加代码：
```xml
uploadArchives {
    repositories {
        mavenDeployer {
            repository(url: "http://127.0.0.1:9999/repository/ijustyce/") {
                authentication(userName: "ijustyce", password: "pw")
            }

            pom.project {
                version '1.0.1'
                artifactId 'fastandroiddev3'
                groupId 'com.ijustyce.fastandroiddev'
                packaging 'aar'
                description 'dependences lib'
            }
        }
    }
}
```
这里假设你新建的仓库名字是ijustyce，用户名也是，密码是pw，然后，终端输入：./gradlew uploadArchives即可，等你上传完毕，你就可以使用它了，只是需要修改project的build.gradle，我的如下：
```xml
allprojects {
    repositories {
        jcenter()
        maven { url "https://jitpack.io" }
        maven {
            url "http://192.168.199.44:9999/repository/ijustyce/"
        }
    }
}
```