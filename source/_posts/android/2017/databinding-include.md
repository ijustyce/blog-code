---
title: databinding里include的误解
date: 2016-11-21 14:13:12
categories: Android
---
或许，我们误解databinding里的include久了，确切说，是我们误解Android布局里的include久了。一直以来，有这样一种声音，databinding里include进来的view，即使你有id，也不生成，你还需要通过findViewById来找它，如果不是include进来的view，如果有id，会自动生成的。真相真的如此？<!--more-->  
## databinding里include的误解
### include的缺陷
假设xml布局如下：
```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:bind="http://schemas.android.com/tools">

    <data class="MainView">

        <import type="com.ijustyce.weekly1601.event.DownloadAndUploadEvent" />
        <import type="com.ijustyce.weekly1601.event.ShowFunctionEvent" />

        <variable
            name="clickEvent"
            type="DownloadAndUploadEvent" />

        <variable
            name="functionEvent"
            type="ShowFunctionEvent" />
    </data>

    <LinearLayout
        android:orientation="vertical"
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <com.ijustyce.fastandroiddev3.ui.CommonTitleBar
            android:id="@+id/titleBar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            app:titleTxt="请选择功能" />

        <include
            android:id="@+id/MainFunction"
            bind:clickEvent="@{clickEvent}"
            bind:functionEvent="@{functionEvent}"
            layout="@layout/view_main_function" />

    </LinearLayout>
</layout>
```
然后在activity里就可以：
```java
MainView contentView = DataBindingUtil.setContentView(this, R.layout.main);
contentView.titleBar.getViewModel().labelTxt.set("欢迎");
```
这个功能很强大，一旦添加id，就可以直接使用这个view了，而include进来的view却没法这么弄，一直以来，我以为这是缺陷。如果你真的了解include，你应该知道，假设你include进来一个布局，它最外层布局的id是topView，然后给include设置的id是include，你通过findViewById(R.id.topView)然后设置背景颜色不会成功！你需要findViewById(R.id.include)然后设置背景颜色。给include添加的属性会直接作用于include进来的view的最外层？我也有这种感觉，如果真是这样，今天也不会有这篇博文了，我更觉得include是一个特殊的容器，假设这里include进来的布局 view_main_function 的代码如下：
```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android">

    <data class="MainViewSecond">

        <import type="com.ijustyce.weekly1601.event.DownloadAndUploadEvent" />
        <import type="com.ijustyce.weekly1601.event.ShowFunctionEvent" />

        <variable
            name="clickEvent"
            type="DownloadAndUploadEvent" />

        <variable
            name="functionEvent"
            type="ShowFunctionEvent" />
    </data>

    <ScrollView
        android：id="@+id/scrollView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

    </ScrollView>
</layout>
```
然后，你发现奇迹出现了，你可以通过：contentView.MainFunction.scrollView来访问这里的ScrollView了，当你键入 contentView.MainFunction编辑器就会提示，然后你会看到contentView.MainFunction的类型是：MainViewSecond，这给我的感觉是include是一个特殊的容器，所以，可以通过上面的java代码访问scrollview，或者说include的id并不是直接作用于view的最外层布局，而是指整个view，总之，include进来的布局是可以访问的，这才是最重要的，至于include的理解或许并不那么重要了，如果用monitor抓取view，会发现include是直接复制布局的，看不见include的影子。