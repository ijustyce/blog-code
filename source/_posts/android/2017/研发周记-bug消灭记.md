---
title: 研发周记-bug消灭记
toc: true
date: 2017-01-05 16:29:24
categories: Android
tag: 研发周记
---
一周又快要结束了，消灭了很多bug，有没有，大家推崇至极的rxandroid却内存溢出，狂崩不已，webview无法在https的链接里加载http的图片，contentprovider有时候总报Unknown Url,最后webview启用硬件加速会导致TimeOutException？让我们一起来看看吧。<!--more-->
## bug消灭记
### 关于rxandroid和rxjava
想必不少人至少听说过它们吧，没听说过也没关系，它就是两个字异步，用它可以轻松的实现线程切换，解决大多数ANR，但是，内存泄露却也很严重！具体请看：[在Android中使用RxJava时如何防止内存泄露](http://www.jianshu.com/p/aedadec2dc1a)以及[你是用rxjava时内存泄露了吗](http://www.jianshu.com/p/c720ec2b5383) 至于rxjava的内存泄露，我最终并没有选择去解决它，再牛的框架，如果不关心自己的质量问题，我也没必要为它填坑！它的内存泄露是很重要的，而不是一点半点，这个内存泄露，导致我app上线后不断崩溃，至少占据总崩溃的六成以上！
### webview的问题
首先有一个bug总是出现，崩溃信息如下：
```java
java.util.concurrent.TimeoutException
android.view.ThreadedRenderer.finalize() timed out after 10 seconds
```
没有更多有用的信息，这崩溃让人摸不着头脑，当然，stackoverflow是万能的，它给出了答案，具体信息请查看[stackoverflow](http://stackoverflow.com/questions/27232275/java-util-concurrent-timeoutexception-android-view-threadedrenderer-finalize)按照它的说法，Android4.4及以上会有问题，并且是Chromium的bug，而它正是Android的webview了，至于解决方案：
```java
if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
    mWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
}
```
不过，经实践证明，这个方案不OK，因为如果你播放视频，你就需要访问硬件，以上代码会导致webview无法播放视频，而且，用了它，继续崩溃，至于解决方案，暂时没有。至于加载图片，那很简单：
```java
if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
    mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
}
```
这是因为Android5.0及以上默认不允许https的链接里加载http的图片，但是，我们可以通过设置解决这个问题！但还是建议不要在https的链接里用http的图片，可以看看 {% post_link linux/2017/nginx配置https %} 配置https还是很简单的！
### contentprovider 报错 Unknown URL
你可以看下ContentResolver，它里面报错Unknown URL是有几种情况的，比如：insert、bulkInsert、delete，update的时候可能报错：Unknown URI，至于抛这个异常的时机，只有一个：
```java
IContentProvider provider = acquireProvider(uri);
if (provider == null) {
    throw new IllegalArgumentException("Unknown URI " + uri);
}
```
其中acquireProvider的代码如下：
```java
public final IContentProvider acquireProvider(Uri uri) {
    if (!SCHEME_CONTENT.equals(uri.getScheme())) {
        return null;
    }
    final String auth = uri.getAuthority();
    if (auth != null) {
        return acquireProvider(mContext, auth);
    }
    return null;
}
```
然后acquireProvider(mContext, auth)方法不能查看，抽象类型…于是，我只能在update、insert的时候try、catch了！
