---
title: 超大型Android APP架构探索
toc: true
date: 2017-05-12 09:42:18
categories: Android
tag: Android
url: exploration-of-super-large-android-app-architecture
---
这里，我说的超大型是指这个APP可以划分为多个模块，比如商品模块、定制模块、订单模块、用户模块、购物车模块等。无论哪个模块都可以单独打包为一个APP，我要做的是，设计一个架构，它允许你随意添加、删除或者替换模块，这似乎类似插件式开发，不过，这跟网上的插件化开发有一定的不同，我不依赖任何已有的插件化解决方案，而是使用设计模式去实现它。<!--more-->如果你想了解插件化解决方案，请看：[各种插件化方案的比较](https://github.com/wequick/Small/blob/master/Android/COMPARISION.md)，本文设计的跟这些插件化方案之间有以下不同：  
1、我是基于接口实现，如果要说设计模式，那么会有：策略模式、依赖倒置模式、观察者模式、装饰者模式等。  
2、我实现的是并行开发，低耦合，没有热更新、热升级等。  
3、我用的仅仅是设计模式，所以开发方面不会有任何不同，这意味着你的风险是0，你不是用框架，而是设计模式。  
## 超大型Android APP架构探索
首先，让我们考虑下，可能的需求：  
1、你的APP足够大，你们团队把它拆分为多个模块，每个小组负责一个模块的开发。  
2、各个模块间可能存在互相调用的情况，比如商品页要跳转到购物车，或者下单页。  
3、有一些共用的资源，比如样式、网络库、工具类等。  
4、可能存在数据交换，比如订单页需要跳转登录，登录完成后需要一些后续操作。  
5、可能还有埋点、使用各种第三方sdk的可能，比如bugtags、umeng统计等。
### 使用策略模式实现模块间互调
策略模式的定义是：定义了算法族，并分别封装起来，让它们之间可以互相替换。这里，我的算法族里只有一个算法，谈不上是正宗的策略模式，但如果考虑到发版，你可以定义两个及以上算法族，比如release、develop、test，你可以让他们互相替换，如此说来也算是策略模式了。让我们步入主题。  
这里，我设计了一个特殊的模块，即通用模块，它里面有通用的资源，比如样式、字符串、图片、工具类、网络库等，还有供各个模块调用的接口，今天的主角就是这些接口拉。我主要设计以下核心类：  
1、RouteManager，路由管理类，用于管理各个模块之间的跳转、传值等，该类在通用模块里。  
2、xxxRoute，比如OrderRoute，实现xxxInterface，即实现这个模块里的跳转、传值等问题，该类由子模块完成。  
3、xxxInterface，子模块的跳转、传值的接口，该类由通用模块定义，由具体子模块实现。  
4、主模块，这个模块依赖所有子模块，每个子模块至少依赖通用模块。主模块里设置RouteManager里的值，这样会有几个变量产生，而且这些变量与APP生命周期一致，你可能担心内存问题，这里，仅仅是变量、类的加载，其内存消耗微乎其微，内存这块，你还是担心下项目里的图片吧。  
至此，主要的模块设计完毕，APP启动后，在Application类的onCreate里判断进程是否是主进程，如果是，设置RouteManager里的值，并注册本地广播，当子模块调用RouteManager里的xxxRoute并发现其是null时，发送广播，Application类收到广播后重新设置RouteManager里的值，理论上无需如此，但如果实在奇葩的出现了为null的情况，则可以如此解决！  
假设A模块需要启动B模块里的Activity、只需要调用RouteManager里的BRoute里对应的方法即可！  
### 使用装饰者模式实现无痛埋点
埋点的痛处在于其破坏了封装，因为我们的代码一般是这样写的：
```java
public void clickBuy() {
    //  你的购买逻辑
}
```
很明显，点击事件和业务耦合了，再简单点，你的点击对应一个业务，而不是把点击分发下去，然后由其他类实现。不过，如果只有埋点，你可以不用装饰者模式，我假定你用的是mvvm，以上代码写在GoodsEvent中，你可以再写一个GoodsTrackEvent，它继承GoodsEvent，然后重写clickBuy，你先调用父类的clickBuy然后调用埋点，OK了！是的，通过继承，我们就可以实现无痛埋点了，但是继承有一个缺点，子类的父类是确定的，不能实现动态的继承，即A继承B，那么，A里调用super.xxx其实就是调用B里的方法，做不到调用C里的方法，于是有了装饰者模式。不过，这里，如果仅仅是埋点，你可以直接用继承，但我推荐装饰者模式，因为它更灵活，耦合更低。  
装饰者模式的定义是：将责任动态的附加在对象身上，比继承更具弹性。我们看一段代码：  
main.java
```java
public class Main {

    public static void main(String[] args) {

        Computer thinkPad520 = new ThinkPad("ThinkPad 520");

        Computer noHardWare = new NoHardWare(thinkPad520);
        noHardWare.printInformation();

        Computer withCpu = new Cpu(thinkPad520);
        withCpu.printInformation();

        Computer withCpuAndMemory = new Memory(withCpu);
        withCpuAndMemory.printInformation();

        Computer withCpuAndMemoryAndDisk = new Disk(withCpuAndMemory);
        withCpuAndMemoryAndDisk.printInformation();
    }
}
```
Computer.java
```java
public abstract class Computer {

    public String description = "Unknown";

    public String getDescription() {
        return description;
    }

    public abstract float getPrice();

    public final void printInformation() {
        System.out.println(getDescription() + ", total price is " + getPrice() + "\n");
    }
}
```
ThinkPad.java
```java
public class ThinkPad extends Computer {

    public ThinkPad(String description) {
        this.description = description;
    }

    @Override
    public float getPrice() {
        return 5545;
    }
}
```
NoHardWare.java
```java
public class NoHardWare extends ComputerDecorator {

    public NoHardWare(Computer computer) {
        super(computer);
    }

    @Override
    public float getPrice() {
        return computer.getPrice();
    }

    @Override
    public String getDescription() {
        return computer.getDescription();
    }
}
```
ComputerDecorator.java
```java
public abstract class ComputerDecorator extends Computer{

    public Computer computer;

    public ComputerDecorator(Computer computer) {
        this.computer = computer;
    }
}
```
Cpu.java
```java
public class Cpu extends ComputerDecorator {

    @Override
    public String getDescription() {
        return computer.getDescription() + ", Cpu";
    }

    public Cpu(Computer computer) {
        super(computer);
    }

    @Override
    public float getPrice() {
        return 820 + computer.getPrice();
    }
}
```
Disk.java
```java
public class Disk extends ComputerDecorator {

    public Disk(Computer computer) {
        super(computer);
    }

    @Override
    public float getPrice() {
        return computer.getPrice() + 560;
    }

    @Override
    public String getDescription() {
        return computer.getDescription() + ", disk";
    }
}
```
Memory.java
```java
public class Memory extends ComputerDecorator {

    public Memory(Computer computer) {
        super(computer);
    }

    @Override
    public float getPrice() {
        return computer.getPrice() + 435;
    }

    @Override
    public String getDescription() {
        return computer.getDescription() + ", 8GB memory";
    }
}
```
代码可能有点多，电脑类用了工厂方法，你也可以理解为依赖倒置，ThinkPad继承自电脑，并实现了描述和价格两个方法，电脑装饰者继承自电脑，且含有一个电脑对象，然后cpu、Memory、Disk均继承自电脑装饰者，如此，除了Main和电脑，其他类均继承自电脑，所以都可以使用电脑装饰者，再看看最后的内存类，它计算价格仅仅是返回自己的价格和当前的电脑的价格，当前的电脑可以是各种叠加，比如仅仅是电脑，也可以是电脑和硬盘的叠加，很显然，使用装饰者模式，你可以随意组合对象，比如，你只买电脑和硬盘，或者电脑和内存，如果通过继承，那么你会累死，各种组合，代码量也是相当恐怖的，从以上的代码，你可以看出，装饰者模式，可以随意把特定的对象组合，这里的特定指这些对象均继承自这个装饰者类。如果用这个模式实现埋点，可能并不如使用继承，而它的优势也没提现出来，毕竟组合少！同样的，第三方sdk，比如都需要在oncreate里调用方法啥的，你可以使用装饰者模式，这时候用装饰者模式再好不过了。
### 最后的总结
开发一个App，尤其是你刚接手这个app，那么，你需要设计好架构，充分考虑到扩展性和耦合，让自己的系统更具弹性，简单的说，如果不需要埋点，那么这个app应该还不至于太大，我的经历是这样的，如果很大，需要多模块开发，那么，你可以考虑使用策略模式，可以随意的卸载添加，保证自己的app可以并行开发。每个模块里面，考虑到扩展，比如埋点、比如第三方sdk引入。说了这么多，其实就只有封装和接口四个字！