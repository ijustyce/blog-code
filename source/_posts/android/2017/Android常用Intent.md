---
title: Android常用Intent
toc: true
date: 2016-11-04 12:22:08
categories: Android
---
在上一篇博客里提到了补充Android常用的intent，比如打开地图、发送邮件。本文就是为此而写，将会不断更新。目前包括: 发送邮件、打开地图、选择图片、拍照、裁剪图片。<!--more-->
### 发送邮件：
``` java
public static boolean sendEmail(String email, String title, String content, Context mContext) {

    Intent mIntent = new Intent(Intent.ACTION_SENDTO);
    mIntent.setData(Uri.parse("mailto:" + email));
    if (!StringUtils.isEmpty(title)) {
        mIntent.putExtra(Intent.EXTRA_SUBJECT, title);
    }
    if (!StringUtils.isEmpty(content)) {
        mIntent.putExtra(Intent.EXTRA_TEXT, content);
    }
    if (mIntent.resolveActivity(mContext.getPackageManager()) == null) return false;
    Intent chooserIntent = Intent.createChooser(mIntent, "请选择您需要打开的软件");    //  没有软件可执行这个操作
    mContext.startActivity(chooserIntent);
    return true;
}
```
### 打开地图并展示某个点
``` java
public static boolean viewInMap(double latitude, double longitude, String addressName, Context mContext) {

    Uri mUri = Uri.parse("geo:" + latitude + "," + longitude + "?q=" + addressName);
    Intent mIntent = new Intent(Intent.ACTION_VIEW, mUri);
    if (mIntent.resolveActivity(mContext.getPackageManager()) == null)
        return false; // 没有软件可执行这个操作
    Intent chooserIntent = Intent.createChooser(mIntent, "请选择您需要打开的软件");
    mContext.startActivity(chooserIntent);
    return true;
}
```
### 选择图片
``` java
private void selectPicture() {

    Intent mIntent = new Intent(Intent.ACTION_PICK, null);
    mIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
    if (mIntent.resolveActivity(mActivity.getPackageManager()) == null) {
        ToastUtil.shortToast(R.string.image_pick_failed);
        return;
    }
    Intent chooserIntent = Intent.createChooser(mIntent, "请选择您需要打开的软件");    //  没有软件可执行这个操作
    startActivityForResult(chooserIntent, IMAGE_PICK);
}
```
### 拍照
``` java
private void takePhoto() {
    if (!requestPermission()) return;
    Intent mIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    mIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(
            Environment.getExternalStorageDirectory(), DEFAULT_IMG_FILE)));
    if (mIntent.resolveActivity(mActivity.getPackageManager()) == null) {
        ToastUtil.shortToast(getContext(), R.string.image_capture_failed);
        return;
    }
    Intent chooserIntent = Intent.createChooser(mIntent, "请选择您需要打开的软件");    //  没有软件可执行这个操作
    startActivityForResult(chooserIntent, TAKE_PHOTO);// 采用ForResult打开
}
```
拍照需要动态授权，即运行时权限，这部分，我在下篇继续讲述吧。
### 裁剪照片
``` java
public void cropPhoto(Uri uri) {
    Intent intent = new Intent("com.android.camera.action.CROP");
    intent.setDataAndType(uri, "image/*");
    intent.putExtra("crop", "true");
    // aspectX aspectY 是宽高的比例
    intent.putExtra("aspectX", 1);
    intent.putExtra("aspectY", 1);
    // outputX outputY 是裁剪图片宽高
    intent.putExtra("outputX", 96);
    intent.putExtra("outputY", 96);
//            intent.putExtra("outputX", DensityUtil.dip2px(getActivity(), 150));   //  太大了
//            intent.putExtra("outputY", DensityUtil.dip2px(getActivity(), 150));
    intent.putExtra("return-data", true);
    if (intent.resolveActivity(mActivity.getPackageManager()) == null) {
        ToastUtil.shortToast(getContext(), R.string.crop_failed);
        return;
    }
    Intent chooserIntent = Intent.createChooser(intent, "请选择您需要打开的软件");    //  没有软件可执行这个操作
    startActivityForResult(chooserIntent, CROP_PHOTO);
}
```
### 打开某个网址
``` java
public static boolean openUrl(String url, Activity mContext) {

    if (mContext == null || !RegularUtils.isCommonUrl(url)) return false;

    Intent intent = new Intent();
    intent.setAction("android.intent.action.VIEW");
    Uri content_url = Uri.parse(url);
    intent.setData(content_url);
    if (intent.resolveActivity(mContext.getPackageManager()) == null) return false;
    mContext.startActivity(Intent.createChooser(intent, "请选择应用"));
    return true;
}
```
### 选择文件
``` java
public static boolean chooseFile(Activity mContext, String type, int requestCode) {

     if (mContext == null) return false;
     Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
     intent.setType(type == null ? "*/*" : type + "/*");
     intent.addCategory(Intent.CATEGORY_OPENABLE);
     if (intent.resolveActivity(mContext.getPackageManager()) == null) {
         return false;
     }
     Intent chooserIntent = Intent.createChooser(intent, "请选择文件");
     mContext.startActivityForResult(chooserIntent, requestCode);
     return true;
 }
 ```
### 调用系统分享
 ``` java
 public static boolean systemShare(Context mContext, String text) {

    if (mContext == null || StringUtils.isEmpty(text)) return false;
    Intent intent = new Intent(Intent.ACTION_SEND);
    //  intent.putExtra(Intent.EXTRA_SUBJECT, text);
    intent.putExtra(Intent.EXTRA_TEXT, text);
    intent.setType("text/plain");
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    if (intent.resolveActivity(mContext.getPackageManager()) == null) return false;
    mContext.startActivity(Intent.createChooser(intent, mContext.getResources().
            getText(R.string.select_app)));
    return true;
}

public static boolean systemShare(Context mContext, String text,
                               String filePath) {

    if (mContext == null || (StringUtils.isEmpty(text) && StringUtils.isEmpty(filePath))) return false;
    File f = new File(filePath);
    if (!f.exists()) return false;
    Intent intent = new Intent(Intent.ACTION_SEND);
    intent.setType("image/*");
    intent.putExtra(Intent.EXTRA_TEXT, text);
    intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    if (intent.resolveActivity(mContext.getPackageManager()) == null) return false;
    mContext.startActivity(Intent.createChooser(intent, mContext.getResources().
            getText(R.string.select_app)));
    return true;
}
```
### 拨打电话
``` java
public static boolean callPhone(Context mContext, String phoneNumber) {

    if (mContext == null || StringUtils.isEmpty(phoneNumber)) return false;
    Uri uri = Uri.parse("tel:" + phoneNumber);
    Intent call = new Intent(Intent.ACTION_CALL, uri);
    if (call.resolveActivity(mContext.getPackageManager()) == null) {
        return toCallPhoneActivity(mContext, phoneNumber);
    }
    mContext.startActivity(Intent.createChooser(call, "请选择您需要打开的软件！"));
    return true;
}

public static boolean toCallPhoneActivity(Context mContext, String phoneNumber) {

    if (mContext == null || StringUtils.isEmpty(phoneNumber)) return false;
    Uri uri = Uri.parse("tel:" + phoneNumber);
    Intent call = new Intent(Intent.ACTION_DIAL, uri);
    if (call.resolveActivity(mContext.getPackageManager()) == null) {
        return false;
    }
    mContext.startActivity(Intent.createChooser(call, "请选择您需要打开的软件！"));
    return true;
}
```
### 发送短信
``` java
public static boolean toSendMessageActivity(Context mContext, String strPhone, String strMsg) {

    if (mContext == null || StringUtils.isEmpty(strPhone) || StringUtils.isEmpty(strMsg))
        return false;
    if (PhoneNumberUtils.isGlobalPhoneNumber(strPhone)) {
        Uri uri = Uri.parse("smsto:" + strPhone);
        Intent sendIntent = new Intent(Intent.ACTION_VIEW, uri);
        if (!StringUtils.isEmpty(strMsg)) {
            sendIntent.putExtra("sms_body", strMsg);
        }
        if (sendIntent.resolveActivity(mContext.getPackageManager()) == null) {
            return false;
        }
        mContext.startActivity(Intent.createChooser(sendIntent, "请选择您需要打开的软件！"));
    }
    return true;
}
```
### 打开当前应用的通知权限设置
``` java
public static boolean setNotify(Context context) {
    if (context == null) return false;

    Intent intent = new Intent();
    intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
    intent.putExtra("app_package", context.getPackageName());
    intent.putExtra("app_uid", context.getApplicationInfo().uid);
    if (intent.resolveActivity(context.getPackageManager()) == null) {
        return false;
    }
    context.startActivity(Intent.createChooser(intent, "请选择您需要打开的软件！"));
    return true;
}
```
这部分还设计到一段代码，检测自己的应用是否有通知权限，请允许在稍后的博文里补充。
### 查看当前应用的详情
``` java
public static boolean viewCurrentAppDetail(Context context) {
    if (context == null) return false;

    Intent intent = new Intent();
    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
    intent.setData(Uri.fromParts("package", context.getPackageName(), null));
    if (intent.resolveActivity(context.getPackageManager()) == null) {
        return false;
    }
    context.startActivity(Intent.createChooser(intent, "请选择您需要打开的软件！"));
    return true;
}
```
