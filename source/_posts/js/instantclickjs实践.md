---
title: instantclickjs实践
toc: true
date: 2016-11-18 16:51:11
categories: hexo
---
instantclickjs是一款网页预加载利器，在鼠标经过某个URL的时候开始为你预加载，我们点击一个url，一般是鼠标停留在链接上面，然后按下鼠标左键，然后才开始加载，而用了它，在你鼠标经过的时候就已经预加载了。<!--more-->
## 使用instantclickjs
### instantclickjs相关的资源
[官网](http://instantclick.io/)
[github](https://github.com/dieulot/instantclick)
可以直接从官网下载，然后使用即可。
### 最简单的使用
就如官网所言，最简单的使用就是在页面的最后插入相应的js，代码如下：
```js
<script src="instantclick.min.js" data-no-instant></script>
<script data-no-instant>InstantClick.init();</script>
</body>
</html>
```
### 解决兼容问题
instantclick的优点不言而喻，可以加快页面打开速度，缺点也很明显，由于提前缓存了js，导致js方法调用失效。比如百度统计、谷歌统计。本文下面的例子以官方的代码以及多说评论说起。
```js
<script src="instantclick.min.js" data-no-instant></script>
<script data-no-instant>
/* Google Analytics code here, without ga('send', 'pageview') */

InstantClick.on('change', function() {
  ga('send', 'pageview', location.pathname + location.search);
});

InstantClick.init();
</script>
```
这是一段官方的例子，用于兼容谷歌统计，跟之前的代码相比，多了如下代码：
```js
InstantClick.on('change', function() {
  ga('send', 'pageview', location.pathname + location.search);
});
```
我们只需如法炮制，比如，我需要多说，那我的代码改为如下：
```js
<script src="instantclick.min.js" data-no-instant></script>
<script data-no-instant>
/* Google Analytics code here, without ga('send', 'pageview') */

InstantClick.on("change",function(){
    myEvent(window,'load', doEvent());
    if(typeof DUOSHUO !== "undefined") {
      if($(".ds-thread").length) {
        DUOSHUO.EmbedThread($(".ds-thread")[0]);
      }
      var length = $(".ds-thread-count").length;
      for(var index = 0; index < length; index++) {
        DUOSHUO.ThreadCount($(".ds-thread-count")[index]);
      }
    }
  });

InstantClick.init();
```
其中myevent是本博客滚动到最上面需要的，以下则是显示多说评论框以及显示有几条评论用的。
### 修改默认样式
你可以随意定义你需要的样式，css如下：
```css
#instantclick-bar {
  background: white;
}
```
这里，你可以随意定义颜色，甚至不显示。
### 黑名单与白名单
加入这个属性，data-no-instant，则不缓存，data-instant则缓存，这部分我没有深入研究，可以参考[这里](http://instantclick.io/blacklisting)
### 总结
首先，你按正常的引入js，然后添加事件即可，就如我添加的多说一样。由于缓存，js的方法可能没有调用，或者失败，所以你需要自己在它的初始化回调里调用了。
