---
title: appium入门
date: 2016-09-29 16:38:31
categories: Android
toc: true
tag: appium
---
Android自动化测试工具有很多，我用过的有UiAutomator，但是它并不支持webView以及中文输入，今天介绍的Appium支持webView以及中文输入，它的强大还不仅于此，感兴趣的可以自己去搜索资料，这里不再介绍，下面进入主题，Appium入门！<!--more-->
## 安装Appium
### 我的环境
系统：Mac os 10.11
IDEA：intellij idea 2016
### Appium的相关网站
[官网](http://appium.io/)
[下载链接](https://bitbucket.org/appium/appium.app/downloads/)
[github](https://github.com/appium/appium)
当然下载最好去官网，那里有个大大的Download Appium按钮的！如果不愿意自己编译安装，可以忽略github，如果不是找历史版本可以忽略第两个链接。
### Appium的安装
网上的教程大多过久，都是基于源码安装，我推荐下载安装，不过我也贴上源码安装的教程，如果你不愿意源码安装，可以跳过这节了。
首先确保你安装了npm（node.js的包管理器）、node.js,如果没安装，则下载安装，[node.js官网](https://nodejs.org/en/)安装好node.js npm也就
安装好了，node.js附带npm！源码安装的最大障碍是防火墙，所以我们用淘宝镜像。在用户目录下，有个.npmrc文件（如果没有就创建），打开后添加
``` bash
registry=https://registry.npm.taobao.org/
sass_binary_site=https://npm.taobao.org/mirrors/node-sass/
```
接着输入：
``` bash
npm install -g appium --registry=https://registry.npm.taobao.org
```
这里之所以还指定镜像是怕前面的修改没工作，比如需要重启或者需要source下！如果提示错误，可以去Appium的github看下issue，或者google下，我接的有个权限问题，记不清了！如果实在搞不定，就用官方的包吧，毕竟那很简单，也是稳定版本！
### 验证安装
如果是直接安装的dmg文件，可以忽略这节，如果是npm安装的，在终端输入appium看下结果！会显示版本信息的，我安装的是1.5.3，npm以及pkg包都安装了…
## 测试Android App
虽然，这里我是测试Android，你也可以看看，它同样支持ios
### 创建maven项目
先创建一个maven项目，然后添加依赖等，我的pom文件如下，可以考虑使用阿里的镜像库，不然慢死你！
``` xml
<dependencies>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.seleniumhq.selenium</groupId>
        <artifactId>selenium-java</artifactId>
        <version>2.53.1</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>io.appium</groupId>
        <artifactId>java-client</artifactId>
        <version>4.1.2</version>
    </dependency>
    <dependency>
        <groupId>com.googlecode.json-simple</groupId>
        <artifactId>json-simple</artifactId>
        <version>1.1.1</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>commons-lang</groupId>
        <artifactId>commons-lang</artifactId>
        <version>2.6</version>
        <scope>test</scope>
    </dependency>
    <!-- Includes the Sauce JUnit helper libraries -->
    <dependency>
        <groupId>com.saucelabs</groupId>
        <artifactId>sauce_junit</artifactId>
        <version>2.1.21</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>com.google.code.gson</groupId>
        <artifactId>gson</artifactId>
        <version>2.7</version>
    </dependency>
</dependencies>

<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-surefire-plugin</artifactId>
        </plugin>
        <plugin>
            <artifactId>maven-compiler-plugin</artifactId>
            <configuration>
                <source>1.8</source>
                <target>1.8</target>
            </configuration>
        </plugin>
    </plugins>
</build>
```
创建后先写个hello appium，哈哈，然后开始一个测试，以下代码可以让你跑起来，但仅仅是安装，启动，没别的操作！
``` java
DesiredCapabilities capabilities = new DesiredCapabilities();
capabilities.setCapability("platformName", "Android");
capabilities.setCapability("platformVersion", "4.4");
capabilities.setCapability("deviceName","Android Emulator");
capabilities.setCapability("appFile", appFile.getAbsolutePath());
capabilities.setCapability("appPackage", "com.lzhplus.lzh");
capabilities.setCapability("appActivity", ".ui.activity.SplashActivity");
capabilities.setCapability("noReset", !resetApp);
capabilities.setCapability("unicodeKeyboard", true);
capabilities.setCapability("resetKeyboard", true);
driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
```
这里，你需要修改appPackage以及AppActivity、另外，appFile以及resetApp是我自定义的变量，resetApp指是否清空App数据，倒数第二三行代码用于中文输入，最后一行代码，用于连接到Appium服务器，在测试之前，你需要在终端运行appium或者打开appium软件，同时确保adb命令可用，而且连接的设备（手机或者模拟机）的版本、名称跟上面的4.4以及Android Emulator一致！然后运行程序即可！这里的url是统一的，你不需要修改，包括wd、hub，至于为什么是这个url，Appium启动后，你看下终端的输出你就明白了！
另外，如果你感兴趣，可以看下接下来的文章。
