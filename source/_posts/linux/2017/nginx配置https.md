---
title: nginx配置https
toc: true
date: 2016-11-18 21:39:52
categories: Linux
tag: nginx
---
本文主要讲述nginx下如何配置https，下篇博文介绍retrofit2支持https，我需要的效果是nginx同时监听https和http，访问http的时候自动跳转https<!--more-->
## nginx配置https
本文主要参考了两篇博文，然后加上自己的实践而成，这两篇博文分别是：[自己制作https/ssl证书for nginx/lighttpd/apache（chrome不会报错）](http://blog.yunqi.li/archives/make-a-fake-ssl-cert-for-your-site.html)和[Nginx server之Nginx添加ssl支持](https://my.oschina.net/zijian1315/blog/207311),我先补充一句，chrome导入的时候需要选择.pem文件！
### 生成必要的文件
```bash
openssl genrsa -des3 -out tmp.key 2048
openssl rsa -in tmp.key -out ssl.key
openssl req -new -key ssl.key -out ssl.csr
```
最后一条命令需要你填写很多信息，其中，Common Name是你的https的域名，我暂时没有测试过如何支持泛域名，(我已经尝试了泛域名，需要的请看 {% post_link linux/2017/openssl-multiple-common-names %} )challenge password这项不填写。这两条你需要牢记！接着：
```bash
openssl x509 -req -days 3650 -in ssl.csr -signkey ssl.key -out ssl.crt
cat ssl.key ssl.crt > ssl.pem
```
至此，我们生成了四个文件：ssl.key、ssl.csr、ssl.crt、ssl.pem，接下来配置nginx。
### nginx配置https支持
```nginx
user  nginx;
worker_processes  2;
events {
    worker_connections  1024;
}

http {
        include       mime.types;
        default_type  application/octet-stream;
        log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
        sendfile        on;
        tcp_nopush      on;
        keepalive_timeout  65;
        gzip  on;

server {
        listen       443;
        server_name  ssl.ijustyce.win;
        charset uft-8;
        root /var/www/html;
        location / {
             index index.html index.htm;
        }
        ssl on;
        ssl_certificate /home/android/java-web/ssl.pem;
        ssl_certificate_key /home/android/java-web/ssl.key;
      }

server {
        listen       80;
        server_name  ssl.ijustyce.win;
        charset uft-8;
        root /var/www/html;
        location / {
            index index.html index.htm;
        }
      }
}
```bash
其中ssl.ijustyce.win是我配置的Common Name，至此，配置结束。终端输入：sudo nginx -s reload然后就可以访问了。然后，再来实现一个功能，http自动跳转https，404跳转首页。
```bash
server {
        listen       80;
        server_name  ssl.ijustyce.win;
        rewrite ^(.*)$  https://$host$1 permanent;
      }
```
404跳转首页，只需要在server下面添加：
```bash
error_page  404 https://ssl.ijustyce.win/;
```
至此，nginx已支持https，接下来，我们打开chrome，然后打开设置，显示高级设置，管理证书，新弹出的页面切换到授权中心，导入，选择生成的ssl.pem文件，然后勾选第一项，信任网站身份，或者全选。然后chrome打开，发现没有警告了，下一篇博文，我将讲述retrofit如何使用https，敬请期待。
