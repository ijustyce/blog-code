---
title: openssl的那些事
toc: true
date: 2016-11-19 19:10:03
categories: Linux
---
在 {% post_link linux/2017/nginx配置https %} 一文中，我写了好几条openssl指令，但是我也不明白啥意思，只知道是生成证书用的，今天的博文，我要整理下这些命令，希望自己能更好的理解，更好的运用它。
##　openssl的那些事
### rsa指令
把rsa私有密钥文件的保护密码去掉，但是不推荐这么做，命令如下：
```shell
openssl rsa -in tmp.key -out ssl.key
```
用rsa算法加密私有密钥文件
```shell
openssl rsa -in tmp.key -des3 -out ssl.key
```
提取公钥
```shell
openssl rsa -in tmp.key -pubout -out pubkey.key
```
生成rsa私钥(推荐使用2048位)
```shell
genrsa -out rsa_private_key.key 1024
```
通过私钥生成rsa公钥
```shell
openssl rsa -in rsa_private_key.key -pubout -out rsa_public_key.key
```
### 生成证书签署请求
```shell
openssl req -new -key ssl.key -out ssl.csr
```
这一步会要求你填写很多信息，Common Name你可以用类似*.ijustyce.win的通配符，challenge password这项不填。
### 签署证书
```shell
openssl x509 -req -days 3650 -in ssl.csr -signkey ssl.key -out ssl.crt
cat ssl.key ssl.crt > ssl.pem
```
目前，我只整理了这么点，后期补充ssl双向认证，单向认证服务端是来者不拒的，客户端认证服务端的，我们，不能来着不拒的，下篇博文，我会补充双向认证的。
