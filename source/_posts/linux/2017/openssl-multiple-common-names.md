---
title: openssl配置多域名
toc: true
date: 2016-11-19 23:29:26
categories: Linux
tag: openssl
---
在之前的一篇博文 {% post_link linux/2017/nginx配置https %} 里我曾提到openssl支持泛域名，但是我没有尝试过，刚才，我尝试成功了，主要参考[这篇文章](https://easyengine.io/wordpress-nginx/tutorials/ssl/multidomain-ssl-subject-alternative-names/)下面进入主题。
## openssl配置多个域名
### 生成配置文件：
```bash
cp /etc/ssl/openssl.cnf ./ijustyce.conf
```
然后修改ijustyce.conf文件，找到#req_extensions = v3_req这行，我这里是第125行，如果没有找到，你就新增这行。找到[ v3_req ]这个节点，没有就新增，然后在下面新增：subjectAltName = @alt_names,修改后的代码如下，没有这个节点就自己新增吧。
```bash
[ v3_req ]

# Extensions to add to a certificate request

basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
```
在文件的末尾新增
```bash
[ alt_names ]
DNS.1 = www.example.com
DNS.2 = example.com
DNS.3 = www2.example.com
DNS.4 = 123.example.com
```
这里DNS之后的都是你的域名，如果你的域名很多，你直接列出来，但是保证文件里的commonName_max 对应的值不小于你的域名数量，超了你就修改这个值吧。
### 生成相应文件
这里和 {% post_link linux/2017/nginx配置https %} 里的基本一致，但是第三条命令：openssl req -new -key ssl.key -out ssl.csr要改为：openssl req -new -key ssl.key -out ssl.csr -config ijustyce.conf同时，这条命令会询问你很多问题，在commonName哪里，你需要输入\*.example.com这两点你必须要牢记，不能错。
其实，你会发现，我实现的只是同一个域名的子域名，但是多个一级域名呢，这我暂时没解决办法，我试了输入\*.\*,以及\*.ijustyce.\*都失败了。不过，目前的方案足够可以了，一般的公司也不至于需要多个一级域名吧！
