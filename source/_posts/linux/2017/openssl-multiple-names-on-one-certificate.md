---
title: openssl多域名配置续
toc: true
date: 2016-11-22 13:52:15
categories: Linux
tag: openssl
---
在之前的一篇文章 {% post_link linux/2017/openssl-multiple-common-names openssl配置多域名 %} 中，我实现了一个证书包含多个二级域名的配置，但是我也抛出了一个问题，一个证书如何包含多个顶级域名？虽然这个需求可能并不多见，如果需要，这可能不只是自己签发了，自己签发的，目前常见的是用于移动开发中，但是，我最终找到了解决之法，现记录如下。<!--more-->  
与之前那篇文章 {% post_link linux/2017/openssl-multiple-common-names openssl配置多域名 %} 不同的是，前者我是在ubuntu下测试的，而这篇我是在mac os上进行的，主要参考[一个证书多个域名](http://apetec.com/support/generatesan-csr.htm)。
## openssl多域名配置续
### 创建conf文件
创建conf文件与之前写的博文一样，没变化，这里不赘述，不清楚的请看 {% post_link linux/2017/openssl-multiple-common-names openssl配置多域名 %}
### 生成私钥
```bash
openssl genrsa -out ssl.key 2048
```
### 生成csr文件
```bash
openssl req -new -out ssl.csr -key ssl.key -config ijustyce.cnf
```
### 签名文件
```bash
openssl x509 -req -days 3650 -in ssl.csr -signkey ssl.key -out ssl.crt -extensions v3_req -extfile ijustyce.cnf
```
这条命令跟之前的不一样，多了 -extensions v3_req
### 打包key、crt文件成pk12文件
```bash
openssl pkcs12 -export -in ssl.crt -inkey ssl.key -out ssl.p12
```
### 合并key和crt文件成pem文件
```bash
cat ssl.key ssl.crt > ssl.pem
```
然后导入到mac os系统，打开密钥串访问，选中系统，然后解锁系统，然后导入ssl.p12文件，然后最主要的一步是：双击你导入的证书，在使用此证书时，选择始终信任，然后试试吧。
