---
title: tomcat配置https
toc: true
date: 2016-11-19 17:54:46
categories: Linux
tag: openssl
---
上篇文章，我写了retrofit如何使用自定义的证书访问https，上上篇博文里写了nginx如何实现https访问，现在我要说的是tomcat里如何实现https访问。
## tomcat实现https访问
首先，还需要生成一个证书，虽然上上篇博文里已经生成了四个(没看过上上篇的推荐看下 {% post_link linux/2017/nginx配置https %} )，不过现在，我还需要一个，命令如下
```shell
openssl pkcs12 -export -clcerts -inkey ssl.pem -in ssl.crt -out ssl.p12
```
这一步需要一个密码，你可以随便设置，之后在配置tomcat的时候需要，假设你的密码是1234，现在，我们配置tomcat，打开conf目录下的server.xml文件，然后修改配置如下：
```xml
<Connector port="8082" protocol="HTTP/1.1"
           connectionTimeout="20000"
           proxyPort="443"
           SSLEnabled="true"
           maxThreads="150"
           scheme="https"
           secure="true"
           clientAuth="false"
           sslProtocol="TLS"
           keystoreFile="/home/android/java-web/ssl.p12"
           keystorePass="1234"
           keystoreType="PKCS12"
           keyAlias="1"
           SSLEngine="on" SSLVerifyDepth="10"
           redirectPort="8443" URIEncoding="UTF-8"/>
```
这里，8082是我修改的端口，原本是8080的，你需要注意keystoreFile的路径以及keystorePass和keyAlias，keyAlias默认是1，你可以通过下条命令查看：
```shell
keytool -list -v -keystore ssl.p12
```
这会要求你输入密码，输入密码后就可以看到别名了。至此，tomcat也支持https访问了。
